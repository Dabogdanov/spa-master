//
//  Session.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Session.h"

@implementation Session

- (void)invalidate {
  self.token = nil;
  self.servant = nil;
}

@end
