//
//  RoundSpaButton.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 1/13/15.
//  Copyright (c) 2015 Ilya Chistyakov. All rights reserved.
//

#import "RoundSpaButton.h"

@implementation RoundSpaButton

- (CGFloat)cornerRadius {
  return CGRectGetWidth(self.bounds) / 2.0f;
}

@end
