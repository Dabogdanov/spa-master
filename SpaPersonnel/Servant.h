//
//  Servant.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface Servant : NSObject
@property (assign, nonatomic) int64_t ID;
@property (copy, nonatomic) NSString *fullName;
@property (copy, nonatomic) NSString *shortName;

+ (RKObjectMapping *)toServantMapping;

@end
