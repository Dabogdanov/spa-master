//
//  ActivityView.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/29/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface ActivityView : UIView {
  IBOutlet UIView * __weak activityIndicatorBackground;
  IBOutlet UIActivityIndicatorView * __weak activityIndicatorView;
}

@end
