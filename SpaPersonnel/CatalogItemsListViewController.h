//
//  CatalogItemsListViewController.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/8/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "CatalogEntity.h"
#import "ServicesFactory.h"

@class CatalogItemsListViewController;

@protocol CatalogItemsListViewControllerDelegate <NSObject>
- (void)catalogItemsListViewController:(CatalogItemsListViewController *)CatalogItemsListViewController
              userDidPickCatalogEntity:(CatalogEntity *)entity;
- (void)catalogItemsListViewControllerUserWantsToDissmiss:(CatalogItemsListViewController *)CatalogItemsListViewController;

@end

@interface CatalogItemsListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
  IBOutlet UITableView *_tableView;
}
@property (weak, nonatomic) id <CatalogItemsListViewControllerDelegate> delegate;
@property (strong, nonatomic) ServicesFactory *services;

@end
