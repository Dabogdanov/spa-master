//
//  SpaPersonnelApplication.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/26/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

static NSTimeInterval const kIdleTimeInterval = 40.0;

extern NSString * const kApplicationDidTimeoutNotification;

// This class is used instead of UIApplication to
// force logout when user doesn't interact with application
// for kIdleTimeInterval seconds.
@interface SpaPersonnelApplication : UIApplication

// Starts tracking user touches. If user doesn't touch screen for kIdleTimeInterval seconds
// kApplicationDidTimeoutNotification is sent and tracking stops.
// Does nothing if tracking is already in progress.
- (void)startTrackingIdleTime;

// Stops tracking user touches without sending kApplicationDidTimeoutNotification
// Does nothing if no tracking is in progress.
- (void)stopTrackingIdleTime;

@end
