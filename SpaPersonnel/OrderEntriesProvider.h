//
//  OrderEntriesProvider.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/23/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServerManager.h"

@interface OrderEntriesProvider : NSObject

- (instancetype)initWithServerManager:(ServerManager *)serverManager date:(NSDate *)date;

- (void)startWithSuccess:(void (^)(NSArray *orderEntries))success failure:(void (^)(NSError *error))failure;

@end
