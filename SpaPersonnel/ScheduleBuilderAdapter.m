//
//  ScheduleBuilderAdapter.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/17/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ScheduleBuilderAdapter.h"
#import "ScheduleBuilder.h"
#import "ScheduleItem.h"

@interface ScheduleBuilderAdapter () <ScheduleBuilderDelegate>
@end

@implementation ScheduleBuilderAdapter {
  void (^completionBlock)(NSArray *);
  Order *order;
  ScheduleBuilder *builder;
  Servant *servant;
}

+ (ScheduleBuilderAdapter *)asyncBuildScheduleItemsWithOrder:(Order *)order completionBlock:(void (^)(NSArray *))completion servant:(Servant *)servant {
  ScheduleBuilderAdapter *adapter = [[ScheduleBuilderAdapter alloc] initWithOrder:order completionBlock:completion servant:servant];
  [adapter start];
  return adapter;
}

- (instancetype)initWithOrder:(Order *)anOrder completionBlock:(void (^)(NSArray *))completion servant:(Servant *)aServant {
  if (self = [super init]) {
    completionBlock = [completion copy];
    order = anOrder;
    servant = aServant;
  }
  return self;
}

- (void)start {
  builder = [[ScheduleBuilder alloc] initWithOrderEntries:order.orderEntries servant:servant];
  builder.delegate = self;
  [builder build];
}

#pragma mark - ScheduleBuilderDelegate

- (void)scheduleBuilder:(ScheduleBuilder *)scheduleBuilder didBuildSchedule:(NSArray *)schedule {
  for (ScheduleItem *item in schedule) {
    item.order = order;
  }
  completionBlock(schedule);
}

@end
