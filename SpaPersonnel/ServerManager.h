//
//  ServerManager.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Session.h"
#import "CatalogEntitiesList.h"
#import "ConfirmedOrderEntry.h"

static NSInteger const QuantityNotSpecified = 0;

extern NSString * const kUserDidAuthorize;
extern NSString * const kUserDidLogout;

@interface ServerManager : NSObject

@property (strong, nonatomic, readonly) Session *session;

- (void)logout;

- (void)authWithPassword:(NSString *)password
                deviceID:(NSString *)deviceID
                 success:(void (^)(Session *session))success
                 failure:(void (^)(NSError *error))failure;

- (void)checkCatalogUpdatesWithVersion:(NSString *)version
                               success:(void (^)(CatalogEntitiesList *entitiesList))success
                               failure:(void (^)(NSError *error))failure;

- (void)fetchOrderEntriesForDate:(NSDate *)date
                         success:(void (^)(NSArray *orderEntries))success
                         failure:(void (^)(NSError *error))failure;

- (void)addOrderEntryToOrderWithID:(int64_t)orderID
                        withItemID:(int64_t)itemID
                           comment:(NSString *)commentary
                          quantity:(NSDecimalNumber *)quantity
                           success:(void (^)(ConfirmedOrderEntry *entry))success
                           failure:(void (^)(NSError *error))failure;

- (void)editOrderEntryWithID:(int64_t)ID
                   newStatus:(OrderEntryStatus)status
                   newItemID:(int64_t)itemID
                  newComment:(NSString *)commentary
                 newQuantity:(NSDecimalNumber *)quantity
                     success:(void (^)(ConfirmedOrderEntry *entry))success
                     failure:(void (^)(NSError *error))failure;

- (void)deleteOrderEntryWithID:(int64_t)ID
                       success:(void (^)(ConfirmedOrderEntry *))success
                       failure:(void (^)(NSError *))failure;

- (void)asyncGetInfoForCustomerWithID:(int64_t)ID
                              success:(void (^)(NSString *HTMLString))success
                              failure:(void (^)(NSError *))failure;

- (void)asyncAppendString:(NSString *)string
  toInfoForCustomerWithID:(int64_t)customerID
                  orderID:(int64_t)orderID
                  success:(void (^)(NSString *))success
                  failure:(void (^)(NSError *))failure;

@end
