//
//  ClientInfoViewController.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/18/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ClientInfoViewController.h"
#import "CustomerManager.h"
#import "UIAlertView+SpaPersonnel.h"
#import "UIColor+SpaPersonnel.h"
#import "NSString+SpaPersonnel.h"
#import "UIView+SpaPersonnel.h"
#import "ViewWithActivityIndicator.h"

static CGFloat const ExpandedNewCommentViewHeight = 145.0f;

static CGFloat const DefaultContentViewWidth = 600.0f;
static CGFloat const DefaultContentViewHeight = 500.0f;

static NSString * const PlaceholderText = @"Введите текст записи";

@interface ClientInfoViewController () <UIAlertViewDelegate>
@property (copy, nonatomic) void (^onAlertConfirm)();

@end

@implementation ClientInfoViewController {
  CustomerManager *customerManager;
  CGFloat defaultNewCommentViewHeight;
  
  int64_t orderID;
  BOOL textFieldPlaceholderEnabled;
  UIColor *defaultTextFieldColor;
  UIAlertView *confirmStatusChangeAlertView;
  
  Customer *currentCustomer;
}

@synthesize frameOffset;

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveKeyboardWillShowNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveKeyboardWillHideNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  defaultTextFieldColor = commentTextView.textColor;
  [self setPlaceholderEnabled:YES forTextView:commentTextView];
  
  self.preferredContentSize = CGSizeMake(DefaultContentViewWidth, DefaultContentViewHeight);
  
  defaultNewCommentViewHeight = actionsViewHeightConstraint.constant;
  
  [cancelButton setTitleColor:[UIColor buttonDefaultRedColor] forState:UIControlStateNormal];
  [cancelButton setTitleColor:[UIColor buttonHighlightedRedColor] forState:UIControlStateHighlighted];
  
  [self setViewExpanded:NO];
  [self updateConfirmButtonAvailabilityForString:commentTextView.text];
  
  [self updateViewsWithCustomer:currentCustomer];
  [self reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [contentView makeCornersRound];
}

- (void)fillWithCustomer:(Customer *)customer orderID:(int64_t)anOrderID {
  currentCustomer = customer;
  customerManager = [self.services newCustomerManagerForCustomer:customer];
  orderID = anOrderID;
  
  if (self.isViewLoaded) {
    [self updateViewsWithCustomer:customer];
    [self reloadData];
  }
}

- (void)updateViewsWithCustomer:(Customer *)customer {
  NSString *sexAndAgeString = [NSString sexAndAgeStringForCustomer:customer];
  NSString *customerNameString = customer.fullName ? customer.fullName : @"";
  clientNameLabel.text = [NSString stringWithFormat:@"%@, %@", customerNameString, sexAndAgeString ? sexAndAgeString : @""];
}

#pragma mark - Notifications

- (void)didReceiveKeyboardWillShowNotification:(NSNotification *)notification {
  NSDictionary *info = [notification userInfo];
  CGRect kbRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];

  UIWindow *window = contentView.window;
  CGRect contentViewFrame = [window convertRect:contentView.frame fromView:contentView.superview];
  CGRect intersection = CGRectIntersection(kbRect, contentViewFrame);
  self.frameOffset = CGPointMake(0, -(CGRectGetHeight(intersection) + 10.0f));
}

- (void)didReceiveKeyboardWillHideNotification:(NSNotification *)notification {
  self.frameOffset = CGPointZero;
}

#pragma mark - Actions

- (IBAction)userDidPressDismissButton:(id)sender {
  [self.delegate clientInfoViewControllerUserWantsToDismiss:self];
}

- (IBAction)userDidPressAddNewCommentButton:(id)sender {
  [self setViewExpanded:YES];
}

- (IBAction)userDidPressConfirmButton:(id)sender {
  [self sendNewComment:commentTextView.text];
}

- (IBAction)userDidPressAbortButton:(id)sender {
  [self setViewExpanded:NO];
}

#pragma mark - Expand And Collapse New Comment View

- (void)setViewExpanded:(BOOL)expand {
  if (expand) {
    [commentTextView becomeFirstResponder];
  } else {
    [commentTextView resignFirstResponder];
  }
  for (UIView * view in newCommentViews) {
    view.hidden = !expand;
  }
  addNewCommentButton.hidden = expand;
  
  [self updateConfirmButtonAvailabilityForString:commentTextView.text];

  CGFloat newAddCommentViewHeight = expand ? ExpandedNewCommentViewHeight : defaultNewCommentViewHeight;
  actionsViewHeightConstraint.constant = newAddCommentViewHeight;
  [self.view layoutIfNeeded];
}

#pragma mark - Loading And Sending Data

- (void)reloadData {
  [((ViewWithActivityIndicator *)self.view) lockInputAndShowActivityIndicator];
  [customerManager asyncGetCommentaryStringWithCompletion:^(NSString *HTMLString, NSError *error) {
    [self handleResponseWithHTMLString:HTMLString error:error];
  }];
}

- (void)sendNewComment:(NSString *)comment {
  [self displayConfirmationAlertWithConfirmationHandler:^{
    [self setViewExpanded:NO];
    [self setPlaceholderEnabled:YES forTextView:commentTextView];
    [self updateConfirmButtonAvailabilityForString:commentTextView.text];
    if (comment.length) {
      [customerManager asyncAppendCommentaryString:comment withOrderID:orderID completion:^(NSString *HTMLString, NSError *error) {
        [self handleResponseWithHTMLString:HTMLString error:error];
      }];
    }
  }];
}

- (void)handleResponseWithHTMLString:(NSString *)HTMLString error:(NSError *)error {
  dispatch_async(dispatch_get_main_queue(), ^{
    [((ViewWithActivityIndicator *)self.view) unlockInputAndHideActivityIndicator];
    if (!error) {
      [webView loadHTMLString:HTMLString baseURL:nil];
    } else {
      [UIAlertView showAlertWithTitle:@"Ошибка" message:error.localizedDescription];
    }
  });
}

#pragma mark - UIAlertViewDelegate

- (void)displayConfirmationAlertWithConfirmationHandler:(void (^)())confirmHandler {
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Вы уверены?" message:nil delegate:self cancelButtonTitle:@"Нет" otherButtonTitles:@"Да", nil];
  confirmStatusChangeAlertView = alertView;
  
  ClientInfoViewController * __weak safeSelfReference = self;
  self.onAlertConfirm = ^{
    confirmHandler();
    safeSelfReference.onAlertConfirm = nil;
  };
  [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if ([confirmStatusChangeAlertView isEqual:alertView]) {
    if (buttonIndex != [alertView cancelButtonIndex]) {
      self.onAlertConfirm();
      confirmStatusChangeAlertView = nil;
    }
  }
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
  NSString *resultString = [textView.text stringByReplacingCharactersInRange:range withString:text];
  [self updateConfirmButtonAvailabilityForString:resultString];
  return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
  if (textFieldPlaceholderEnabled) {
    [self setPlaceholderEnabled:NO forTextView:textView];
  }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
  if (!textView.text.length) {
    [self setPlaceholderEnabled:YES forTextView:textView];
  }
}

// TODO: move to wrapper class
- (void)setPlaceholderEnabled:(BOOL)enabled forTextView:(UITextView *)textView {
  if (enabled) {
    textView.textColor = [UIColor grayColor];
    textView.text = PlaceholderText;
  } else {
    textView.textColor = defaultTextFieldColor;
    textView.text = nil;
  }
  textFieldPlaceholderEnabled = enabled;
}

- (BOOL)canSendString:(NSString *)string {
  return (string.length > 0u) && (!textFieldPlaceholderEnabled);
}

- (void)updateConfirmButtonAvailabilityForString:(NSString *)aString {
  confirmButton.enabled = [self canSendString:aString];
}

@end
