//
//  OrderEntriesArrayBuilder.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/1/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServerManager.h"

@class OrderEntriesArrayBuilder;

@protocol OrderEntriesArrayBuilderDelegate <NSObject>
- (void)orderEntriesArrayBuilder:(OrderEntriesArrayBuilder *)builder didBuildNewEntriesArray:(NSArray *)entries;
- (void)orderEntriesArrayBuilder:(OrderEntriesArrayBuilder *)builder didFailToBuildEntriesArray:(NSError *)error;

@end

@interface OrderEntriesArrayBuilder : NSObject
@property (strong, nonatomic) ServerManager *serverManager;
@property (weak, nonatomic) id <OrderEntriesArrayBuilderDelegate> delegate;

- (instancetype)initWithStartDate:(NSDate *)aStartDate endDate:(NSDate *)anEndDate;

- (void)build;


@end
