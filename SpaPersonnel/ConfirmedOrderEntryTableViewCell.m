//
//  ConfirmedOrderEntryTableViewCell.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/3/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ConfirmedOrderEntryTableViewCell.h"
#import "UIView+SpaPersonnel.h"
#import "UIColor+SpaPersonnel.h"
	
@implementation ConfirmedOrderEntryTableViewCell

- (void)awakeFromNib {
  [cancelledView makeCornersRound];
  [finishedIndicatorView makeCornersRound];
  [servantNameView makeCornersRound];
}

- (void)setControlsState:(ConfirmedOrderControlsState)state {
  for (UIView *controlsSubview in controlsView.subviews) {
    controlsSubview.hidden = (controlsSubview.tag != state);
  }
  if (state == ConfirmedOrderControlsStateIsBeingProcessed) {
    self.contentView.backgroundColor = [UIColor orderScreenItemIsBeginProcessedBackgroudColor];
  } else {
    self.contentView.backgroundColor = [UIColor orderScreenItemBackgroudColor];
  }
}

- (void)setButtonsEnabled:(BOOL)enabled {
  for (UIButton *button in buttons) {
    button.enabled = enabled;
  }
}

- (void)setInfoButtonEnabled:(BOOL)enabled {
  for (UIButton *button in infoButtons) {
    button.hidden = !enabled;
  }
}

- (void)setOpenedClosedDateIntervalString:(NSString *)openedClosedDateIntervalString {
  for (UILabel *label in openedClosedDateIntervalLabels) {
    label.text = openedClosedDateIntervalString;
  }
}

- (void)setQuantityString:(NSString *)quantityString {
  quantityLabel.text = quantityString;
}

#pragma mark - Actions

- (IBAction)userDidPressStart:(id)sender {
  [self.delegate confirmedOrderEntryTableViewCellUserDidPressStartButton:self];
}

- (IBAction)userDidPressFinish:(id)sender {
  [self.delegate confirmedOrderEntryTableViewCellUserDidPressFinishButton:self];
}

- (IBAction)userDidPressCancel:(id)sender {
  [self.delegate confirmedOrderEntryTableViewCellUserDidPressCancelButton:self];
}

- (IBAction)userDidPressInfo:(id)sender {
  [self.delegate confirmedOrderEntryTableViewCell:self userDidPressInfoButton:sender];
}

@end
