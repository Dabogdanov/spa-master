//
//  Servant.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Servant.h"

@implementation Servant

+ (RKObjectMapping *)toServantMapping {
  RKObjectMapping *servantMapping = [[RKObjectMapping alloc] initWithClass:[Servant class]];
  [servantMapping addAttributeMappingsFromDictionary:@{@"ID": @"ID", @"FullName": @"fullName", @"ShortName": @"shortName"}];
  return servantMapping;
}

@end
