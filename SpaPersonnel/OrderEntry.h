//
//  AbstractOrderEntry.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "CatalogEntity.h"

@interface OrderEntry : NSObject
@property (assign, nonatomic) int64_t orderID;
@property (strong, nonatomic) CatalogEntity *item;
@property (strong, nonatomic) NSDecimalNumber *quantity;
@property (copy, nonatomic) NSString *comment;

@end
