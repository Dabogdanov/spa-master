//
//  CatalogItemsListViewController.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/8/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "CatalogItemsListViewController.h"
#import "CatalogCacheManager.h"
#import "UIColor+SpaPersonnel.h"
#import "ViewWithActivityIndicator.h"

static NSString * const CatalogEntityReuseID = @"catalogEntityReuseID";
static NSTimeInterval const SearchDelay = 0.2;

@interface CatalogItemsListViewController () <UISearchResultsUpdating>

@end

@implementation CatalogItemsListViewController {
  NSArray *catalogEntities;
  NSArray *tableModel;
  CatalogCacheManager *cacheManager;
  UISearchController *_searchController;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self updateCatalog];
  [self setupSearchController];
}

- (void)setupSearchController {
  _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
  _searchController.searchResultsUpdater = self;

  [_searchController.searchBar sizeToFit];
  _searchController.searchBar.tintColor = [UIColor whiteColor];
  _searchController.dimsBackgroundDuringPresentation = NO;
  _searchController.searchBar.placeholder = nil;
  _tableView.tableHeaderView = _searchController.searchBar;
  
  // fixes an issue when status bar didn't change color
  // when searchBar moved to UIBarPositionTopAttached position
  _tableView.clipsToBounds = NO;
}

#pragma mark - Actions

- (IBAction)userDidPressDismissController:(id)sender {
  [self.delegate catalogItemsListViewControllerUserWantsToDissmiss:self];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return tableModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  CatalogEntity *entity = tableModel[indexPath.row];
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CatalogEntityReuseID];
  cell.backgroundColor = [UIColor orderScreenHeaderColor];
  cell.textLabel.text = entity.name;
  return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  CatalogEntity *entity = tableModel[indexPath.row];
  
  // if self is in navigation controller hierarchy
  // and delegate pops self out of it
  // if _searchController is active, then
  // search bar will remain on the screen instead of navigation bar
  // application will crash if user taps it.
  [_searchController setActive:NO];
  
  [self.delegate catalogItemsListViewController:self userDidPickCatalogEntity:entity];
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
  NSString *searchString = searchController.searchBar.text;
  [[NSRunLoop currentRunLoop] cancelPerformSelectorsWithTarget:self];
  [self performSelector:@selector(commitSearchWithSearchString:) withObject:searchString afterDelay:SearchDelay];
}

#pragma mark - Table Model

- (void)updateCatalog {
  [((ViewWithActivityIndicator *)self.view) lockInputAndShowActivityIndicator];
  cacheManager = [self.services newCatalogCacheManager];
  [cacheManager checkForUpdatesWithCompletion:^(BOOL success, NSError *error) {
    dispatch_async(dispatch_get_main_queue(), ^{
      [((ViewWithActivityIndicator *)self.view) unlockInputAndHideActivityIndicator];
      if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка обновления каталога"
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:@"ОК"
                                              otherButtonTitles:nil];
        [alert show];
      }
    });
    if (success) {
      [cacheManager fetchCachedCatalogEntitiesWithCompletion:^(NSArray *entities, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
          catalogEntities = [entities sortedArrayUsingComparator:^NSComparisonResult(CatalogEntity *entity1, CatalogEntity *entity2) {
            return [entity1.name localizedCaseInsensitiveCompare:entity2.name];
          }];
          tableModel = catalogEntities;
          [_tableView reloadData];
        });
      }];
    }
  }];
}

- (void)commitSearchWithSearchString:(NSString *)searchString {
  if (searchString.length) {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", searchString];
    tableModel = [catalogEntities filteredArrayUsingPredicate:predicate];
  } else {
    tableModel = catalogEntities;
  }
  [_tableView reloadData];
}

@end
