//
//  PinBuilder.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/18/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "PinBuilder.h"

@implementation PinBuilder {
  NSMutableArray *digits;
  NSUInteger maxPinLength;
}

- (NSUInteger)maxPinLength {
  return maxPinLength;
}

- (instancetype)initWithPinLength:(NSUInteger)length {
  if (self = [super init]) {
    digits = [NSMutableArray new];
    maxPinLength = length;
  }
  return self;
}

- (BOOL)appendDigit:(DecimalDigit *)digit {
  BOOL willAppend = (digits.count < maxPinLength);
  if (willAppend) {
    [digits addObject:digit];
  }
  return willAppend;
}

- (BOOL)removeLastDigit {
  BOOL willRemove = digits.count > 0;
  if (willRemove) {
    [digits removeLastObject];
  }
  return willRemove;
}

- (DecimalDigit *)digitAtIndex:(NSUInteger)idx {
  return [digits objectAtIndex:idx];
}

- (void)clear {
  [digits removeAllObjects];
}

- (NSString *)pinString {
  return [digits componentsJoinedByString:nil];
}

- (NSUInteger)currentDigitCount {
  return digits.count;
}

@end
