//
//  SelectCatalogItemView.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "SelectCatalogItemView.h"

@implementation SelectCatalogItemView

- (NSString *)itemName {
  return itemNameLabel.text;
}

- (void)setItemName:(NSString *)itemName {
  BOOL hasItemName = (_Bool)itemName;
  NSString *text = hasItemName ? itemName : @"Выберите из каталога";
  itemNameLabel.text = text;
  itemNameLabel.textColor = hasItemName ? [UIColor blackColor] : [UIColor lightGrayColor];
}

- (CGSize)sizeThatFits:(CGSize)size {
  CGSize currentSize = self.frame.size;
  CGSize currentLabelSize = itemNameLabel.frame.size;
  CGSize optimumLabelSize = [itemNameLabel sizeThatFits:CGSizeMake(currentLabelSize.width, CGFLOAT_MAX)];
  CGFloat optimumHeightDiff = currentLabelSize.height - optimumLabelSize.height;
  CGSize optimumSize = CGSizeMake(currentSize.width, currentSize.height - optimumHeightDiff);
  return optimumSize;
}

@end
