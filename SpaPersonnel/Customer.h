//
//  Customer.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

typedef NS_ENUM(NSInteger, CustomerSex) {
  CustomerSexMale = 1,
  CustomerSexFemale = 2
};

@interface Customer : NSObject
@property (assign, nonatomic) int64_t ID;
@property (copy, nonatomic) NSString *fullName;
@property (assign, nonatomic) CustomerSex sex;
@property (assign, nonatomic) NSInteger age;

@end
