//
//  OrderEntity.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "OrderEntry.h"
#import "Customer.h"
#import "Servant.h"
#import "CatalogEntity.h"

typedef NS_ENUM(NSInteger, OrderEntryStatus) {
  Cancelled = 0,
  NotStarted,
  InProgress,
  Finished,
  AwaitingForConfirmation,
  OrderEntryStatusCount
};

@interface ConfirmedOrderEntry : OrderEntry
@property (assign, nonatomic) int64_t ID;
@property (assign, nonatomic) int64_t orderID;
@property (strong, nonatomic) Customer *customer;
@property (strong, nonatomic) Servant *servant;
@property (assign, nonatomic) int64_t itemID;
@property (strong, nonatomic) NSDecimalNumber *quantity;
@property (copy, nonatomic) NSString *place;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (assign, nonatomic) NSInteger duration;
@property (copy, nonatomic) NSString *comment;
@property (copy, nonatomic) NSString *price;
@property (assign, nonatomic) OrderEntryStatus status;
@property (strong, nonatomic) NSDate *openedDate;
@property (strong, nonatomic) NSDate *closedDate;
@property (assign, nonatomic) BOOL isCurrent;
@property (assign, nonatomic) CatalogEntityType type;

@end
