//
//  RecommendedOrderEntryTableViewCell.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/3/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "RecommendedOrderEntryTableViewCell.h"
#import "UIView+SpaPersonnel.h"


@implementation RecommendedOrderEntryTableViewCell {
  CGFloat confirmWidthDefault;
  CGFloat cancelWidthDefault;
  CGFloat editWidthDefault;
  CGFloat quantityWidthDefault;
  
  CGFloat confirmPaddingDefault;
  CGFloat cancelPaddingDefault;
  CGFloat editPaddingDefault;
  CGFloat quantityPaddingDefault;
}

- (void)awakeFromNib {
  [quantityLabel makeCornersRound];
}

- (void)setInfoOff {
  [infoButton collapse];
}

- (void)setInfoOn {
  [infoButton expand];
}

- (void)setCancelOff {
  [cancelButton collapse];
}

- (void)setCancelOn {
  [cancelButton expand];
}

- (void)setEditOff {
  [editButton collapse];
}

- (void)setEditOn {
  [editButton expand];
}

- (void)setQuantity:(NSDecimalNumber *)quantity {
  quantityLabel.text = [NSString stringWithFormat:@"%@", quantity];
}

- (void)setButtonsEnabled:(BOOL)enabled {
  editButton.enabled = enabled;
  cancelButton.enabled = enabled;
}

#pragma mark - Actions

- (IBAction)userDidPressCancelButton:(id)sender {
  [self.delegate recommendedOrderEntryTableViewCellUserDidPressCancelButton:self];
}

- (IBAction)userDidPressEditButton:(id)sender {
  [self.delegate recommendedOrderEntryTableViewCellUserDidPressEditButton:self];
}

- (IBAction)userDidPressInfoButton:(id)sender {
  [self.delegate recommendedOrderEntryTableViewCell:self userDidPressInfoButton:sender];
}

@end


@implementation CollapsableButton {
  CGFloat widthDefault;
  CGFloat paddingDefault;
}

- (void)collapse {
  widthConstraint.constant = 0.0f;
  paddingConstraint.constant = 0.0f;
  [self layoutIfNeeded];
}

- (void)expand {
  widthConstraint.constant = widthDefault;
  paddingConstraint.constant = paddingDefault;
  [self layoutIfNeeded];
}

- (void)awakeFromNib {
  widthDefault = widthConstraint.constant;
  paddingDefault = paddingConstraint.constant;
}

@end


@implementation CollapsableLabel {
  CGFloat widthDefault;
  CGFloat paddingDefault;
}

- (void)collapse {
  widthConstraint.constant = 0.0f;
  paddingConstraint.constant = 0.0f;
  [self layoutIfNeeded];
}

- (void)expand {
  widthConstraint.constant = widthDefault;
  paddingConstraint.constant = paddingDefault;
  [self layoutIfNeeded];
}

- (void)awakeFromNib {
  widthDefault = widthConstraint.constant;
  paddingDefault = paddingConstraint.constant;
}

@end


@implementation CollapsableView {
  CGFloat widthDefault;
  CGFloat paddingDefault;
}

- (void)collapse {
  widthConstraint.constant = 0.0f;
  paddingConstraint.constant = 0.0f;
  [self layoutIfNeeded];
}

- (void)expand {
  widthConstraint.constant = widthDefault;
  paddingConstraint.constant = paddingDefault;
  [self layoutIfNeeded];
}

- (void)awakeFromNib {
  widthDefault = widthConstraint.constant;
  paddingDefault = paddingConstraint.constant;
}

@end