//
//  ScheduleBuilderAdapter.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/17/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Order.h"

@interface ScheduleBuilderAdapter : NSObject
+ (ScheduleBuilderAdapter *)asyncBuildScheduleItemsWithOrder:(Order *)order completionBlock:(void (^)(NSArray *))completion servant:(Servant *)servant;

@end
