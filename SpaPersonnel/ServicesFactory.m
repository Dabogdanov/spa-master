//
//  ServicesFactory.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/11/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServicesFactory.h"
#import "ServerManager.h"
#import "AuthorizationManager.h"
#import "ScheduleProvider.h"
#import "OrderManager.h"

@implementation ServicesFactory {
  ServerManager *serverManager;
  AuthorizationManager *authManager;
}

- (instancetype)init {
  if (self = [super init]) {
    serverManager = [[ServerManager alloc] init];
    authManager = [[AuthorizationManager alloc] initWithServerManager:serverManager];
  }
  return self;
}

- (Servant *)servant {
  return serverManager.session.servant;
}

- (void)authWithKey:(NSString *)key completion:(void (^)(NSError *))completion {
  [authManager authWithKey:key completion:^(Session *session, NSError *error) {
    completion(error);
  }];
}

- (void)logout {
  [serverManager logout];
}

- (ScheduleProvider *)newScheduleProvider {
  ScheduleProvider *provider = [[ScheduleProvider alloc] initWithServerManager:serverManager];
  return provider;
}

- (OrderManager *)newOrderManagerForOrder:(Order *)order {
  OrderManager *orderManager = [[OrderManager alloc] initWithOrder:order serverManager:serverManager];
  return orderManager;
}

- (CatalogCacheManager *)newCatalogCacheManager {
  CatalogCacheManager *catalogCacheManager = [[CatalogCacheManager alloc] initWithServerManager:serverManager];
  return catalogCacheManager;
}

- (CustomerManager *)newCustomerManagerForCustomer:(Customer *)customer {
  CustomerManager *customerManager = [[CustomerManager alloc] initWithCustomer:customer serviceManager:serverManager];
  return customerManager;
}

@end
