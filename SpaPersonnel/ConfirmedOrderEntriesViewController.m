//
//  ConfirmedOrderEntriesViewController.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/15/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ConfirmedOrderEntriesViewController.h"
#import "OrderManager.h"
#import "OrderTableModelElement.h"
#import "ConfirmedOrderEntryTableViewCell.h"
#import "UIAlertView+SpaPersonnel.h"
#import "CommentaryViewController.h"

static NSString * const ConfirmedOrderEntryCellID = @"confirmedOrderEntryCellId";

static NSString * const OrderEntryCommentControllerID = @"orderEntryCommentary";

@interface ConfirmedOrderEntriesViewController () <ConfirmedOrderEntryTableViewCellDelegate, UIPopoverControllerDelegate, UIAlertViewDelegate>
@property (copy, nonatomic) void (^onAlertConfirm)();

@end

@implementation ConfirmedOrderEntriesViewController {
  OrderManager *orderManager;
  NSArray *tableModel;
  NSDateFormatter *dateFormatter;
  UIPopoverController *popover;
 
  UIAlertView *confirmStatusChangeAlertView;
  
  Order *order;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  UINib *confirmedCellNib = [UINib nibWithNibName:@"ConfirmedOrderEntryTableViewCell" bundle:nil];
  [self.tableView registerNib:confirmedCellNib forCellReuseIdentifier:ConfirmedOrderEntryCellID];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

- (void)fillWithOrder:(Order *)anOrder {
  order = anOrder;
  orderManager = [self.services newOrderManagerForOrder:order];
  tableModel = [self tableModelWithOrder:order];
  headerView.hidden = !order;
  [self.tableView reloadData];
}

- (NSArray *)tableModelWithOrder:(Order *)order {
  NSMutableArray *newConfirmedElements = [NSMutableArray new];
  for (ConfirmedOrderEntry *entry in order.orderEntries) {
    OrderTableModelElement *element = [[OrderTableModelElement alloc] initWithConfirmedOrderEntry:entry];
    if (element.entry.isCurrent) {
      [newConfirmedElements addObject:element];
    }
  }
  return newConfirmedElements;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return tableModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell;
  OrderTableModelElement *element = tableModel[indexPath.row];
  ConfirmedOrderEntry *entry = element.entry;
  cell = [self cellForCurrentOrderEntry:entry inTableView:tableView];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}

- (ConfirmedOrderEntryTableViewCell *)cellForCurrentOrderEntry:(ConfirmedOrderEntry *)entry inTableView:(UITableView *)aTableView {
  ConfirmedOrderEntryTableViewCell *confirmedEntryCell = [aTableView dequeueReusableCellWithIdentifier:ConfirmedOrderEntryCellID];
  confirmedEntryCell.delegate = self;

  confirmedEntryCell.timeLabel.text = [self timeIntervalStringWithStartDate:entry.startDate endDate:entry.endDate];
  
  confirmedEntryCell.servantNameLabel.text = entry.servant.shortName ? entry.servant.shortName : @"Нет имени мастера";
  confirmedEntryCell.catalogItemNameLabel.text = entry.item ? entry.item.name : [NSString stringWithFormat:@"itemID: %lld", entry.itemID];
  confirmedEntryCell.placeLabel.text = entry.place;

  if (entry.servant && (self.services.servant.ID != entry.servant.ID)) {
    [confirmedEntryCell setControlsState:ConfirmedOrderControlsStateDisplayAssignedServant];
  } else {
    ConfirmedOrderControlsState state = [self cellControlsStateForOrderEntryStatus:entry.status];
    [confirmedEntryCell setControlsState:state];
  }
  
  [confirmedEntryCell setButtonsEnabled:YES];
  
  BOOL commentIsAvailable = (entry.comment.length > 0);
  [confirmedEntryCell setInfoButtonEnabled:commentIsAvailable];
  
  NSString *openedClosedDatesString = nil;
  if (entry.status == InProgress) {
    openedClosedDatesString = [self timeIntervalStringWithStartDate:entry.openedDate endDate:nil];
  } else {
    openedClosedDatesString = [self timeIntervalStringWithStartDate:entry.openedDate endDate:entry.closedDate];
  }
  [confirmedEntryCell setOpenedClosedDateIntervalString:openedClosedDatesString];
  
  NSString *quantityString = [NSString stringWithFormat:@"%@", entry.quantity];
  [confirmedEntryCell setQuantityString:quantityString];
  
  return confirmedEntryCell;
}

- (ConfirmedOrderControlsState)cellControlsStateForOrderEntryStatus:(OrderEntryStatus)status {
  ConfirmedOrderControlsState matchingState = ConfirmedOrderControlsStateIsBeingProcessed;
  switch (status) {
    case Cancelled:
      matchingState = ConfirmedOrderControlsStateCancelled;
      break;
    case NotStarted:
      matchingState = ConfirmedOrderControlsStateUserCanStart;
      break;
    case InProgress:
      matchingState = ConfirmedOrderControlsStateUserCanFinish;
      break;
    case Finished:
      matchingState = ConfirmedOrderControlsStateUserDidFinish;
      break;
    case AwaitingForConfirmation:
      matchingState = ConfirmedOrderControlsStateIsBeingProcessed;
      break;
    default:
      break;
  }
  return matchingState;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 75.0f;
}

#pragma mark - ConfirmedOrderEntryTableViewCellDelegate

- (void)confirmedOrderEntryTableViewCellUserDidPressCancelButton:(ConfirmedOrderEntryTableViewCell *)cell {
  [self displayConfirmationAlertWithConfirmationHandler:^{
    [self setNewStatus:Cancelled ForConfirmedOrderEntryCell:cell];
  }];
}

- (void)confirmedOrderEntryTableViewCellUserDidPressFinishButton:(ConfirmedOrderEntryTableViewCell *)cell {
  [self displayConfirmationAlertWithConfirmationHandler:^{
    [self setNewStatus:Finished ForConfirmedOrderEntryCell:cell];
  }];
}

- (void)confirmedOrderEntryTableViewCellUserDidPressStartButton:(ConfirmedOrderEntryTableViewCell *)cell {
  [self displayConfirmationAlertWithConfirmationHandler:^{
    [self setNewStatus:InProgress ForConfirmedOrderEntryCell:cell];
  }];
}

- (void)confirmedOrderEntryTableViewCell:(ConfirmedOrderEntryTableViewCell *)cell userDidPressInfoButton:(UIButton *)button {
  NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
  OrderTableModelElement *element = tableModel[indexPath.row];
  ConfirmedOrderEntry *entry = element.entry;
  
  UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:OrderEntryCommentControllerID];
  CommentaryViewController *commentaryController = (CommentaryViewController *)navigationController.viewControllers.firstObject;
  [commentaryController fillWithCommentary:entry.comment];
  
  popover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
  [popover presentPopoverFromRect:button.frame inView:button.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
  popover.delegate = self;
}

- (void)setNewStatus:(OrderEntryStatus)status ForConfirmedOrderEntryCell:(ConfirmedOrderEntryTableViewCell *)cell {
  [cell setButtonsEnabled:NO];
  
  NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
  OrderTableModelElement *element = tableModel[indexPath.row];
  ConfirmedOrderEntry *entry = element.entry;
  
  [orderManager asyncEditEntryWithID:entry.ID
                           newItemID:entry.itemID
                       newCommentary:entry.comment
                         newQuantity:entry.quantity
                           newStatus:status
                          completion:^(NSError *error) {
    dispatch_async(dispatch_get_main_queue(), ^{
      if (error) {
        [cell setButtonsEnabled:YES];
        [UIAlertView showAlertWithTitle:@"Ошибка при попытка изменения статуса" message:error.localizedDescription];
      } else {
        
        [self updateRowFromOrderAtIndexPath:indexPath];
        
        [self.delegate orderViewControllerDidChangeOrder:self];
      }
    });
  }];
}

- (void)updateRowFromOrderAtIndexPath:(NSIndexPath *)indexPath {
  tableModel = [self tableModelWithOrder:order];
  [self.tableView beginUpdates];
  [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
  [self.tableView endUpdates];
}

#pragma mark - UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
  popover = nil;
}

#pragma mark - UIAlertViewDelegate

- (void)displayConfirmationAlertWithConfirmationHandler:(void (^)())confirmHandler {
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Вы уверены?" message:nil delegate:self cancelButtonTitle:@"Нет" otherButtonTitles:@"Да", nil];
  confirmStatusChangeAlertView = alertView;
  
  ConfirmedOrderEntriesViewController * __weak safeSelfReference = self;
  self.onAlertConfirm = ^{
    confirmHandler();
    safeSelfReference.onAlertConfirm = nil;
  };
  [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if ([confirmStatusChangeAlertView isEqual:alertView]) {
    if (buttonIndex != [alertView cancelButtonIndex]) {
      self.onAlertConfirm();
      confirmStatusChangeAlertView = nil;
    }
  }
}

#pragma mark - Formatters

- (NSDateFormatter *)dateFormatter {
  if (!dateFormatter) {
    dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"HH:mm";
  }
  return dateFormatter;
}

- (NSString *)timeIntervalStringWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
  NSMutableString *result = [NSMutableString new];
  
  NSString *startDateString = [self stringFromDate:startDate];
  [result appendString:startDateString];
  
  NSString *endDateString = [self stringFromDate:endDate];
  [result appendFormat:@" - %@", endDateString];
  
  return result;
}

- (NSString *)stringFromDate:(NSDate *)date {
  NSString *dateString = @"＿:＿";
  if (date) {
    dateString = [[self dateFormatter] stringFromDate:date];
  }
  return dateString;
}

@end
