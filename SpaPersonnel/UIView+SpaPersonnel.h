//
//  SpaView.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/3/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface UIView (SpaPersonnel)
+ (id)loadFromNibNamed:(NSString *)nibName;

- (void)makeCornersRound;

@end
