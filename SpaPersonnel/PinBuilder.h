//
//  PinBuilder.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/18/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "DecimalDigit.h"

@interface PinBuilder : NSObject
@property (readonly) NSUInteger maxPinLength;

- (instancetype)initWithPinLength:(NSUInteger)length;

// returns NO if pin reached maximum length, otherwise YES
- (BOOL)appendDigit:(DecimalDigit *)digit;
- (BOOL)removeLastDigit;

- (DecimalDigit *)digitAtIndex:(NSUInteger)idx;
- (void)clear;

- (NSString *)pinString;

- (NSUInteger)currentDigitCount;

@end
