//
//  CatalogCache.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/2/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "CatalogCache.h"

@implementation CatalogCache {
  CatalogEntitiesList *memoryCache;
}

- (void)asyncUpdateCacheWithEnitiesList:(CatalogEntitiesList *)entitiesList completion:(void (^)())completion {
  // basically NSCopying copy
  CatalogEntitiesList *newEntitiesList = [[CatalogEntitiesList alloc] init];
  newEntitiesList.catalogVersion = entitiesList.catalogVersion;
  newEntitiesList.catalogEntities = entitiesList.catalogEntities;
  memoryCache = newEntitiesList;
  completion();
}

- (void)asyncGetVersion:(void (^)(NSString *, NSError *))completion {
  completion(memoryCache.catalogVersion, nil);
}

- (void)asyncGetCatalogEntityForID:(int64_t)orderID completion:(void (^)(CatalogEntity *entity, NSError *error))completion {
  NSUInteger index = [memoryCache.catalogEntities indexOfObjectPassingTest:^BOOL(CatalogEntity *entity, NSUInteger idx, BOOL *stop) {
    return entity.ID == orderID;
  }];
  if (index != NSNotFound) {
    completion(memoryCache.catalogEntities[index], nil);
  } else {
    completion(nil, nil); // add error
  }
}

- (void)asyncGetCatalogEntitiesWithCompletion:(void (^)(NSArray *, NSError *))completion {
  completion(memoryCache.catalogEntities, nil);
}

@end
