//
//  SelectCatalogItemTableViewCell.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "SelectCatalogItemTableViewCell.h"

@implementation SelectCatalogItemTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
  if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"SelectCatalogItemView" owner:self options:nil];
    SelectCatalogItemView *view = (SelectCatalogItemView *)views.firstObject;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:view];
    
    self.theContentView = view;
    
    NSArray *constrs = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[view]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)];
    [self.contentView addConstraints:constrs];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)]];
  }
  return self;
}

- (void)setItemName:(NSString *)name {
  [self.theContentView setItemName:name];
  [self updateConstraints];
}

@end
