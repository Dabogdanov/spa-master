//
//  OrderManager.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/10/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Order.h"
#import "ServerManager.h"

@class OrderManager;

@interface OrderManager : NSObject
- (instancetype)initWithOrder:(Order *)anOrder serverManager:(ServerManager *)aServerManager;
- (void)asyncAddOrderEntryWithItemID:(int64_t)itemID
                          commentary:(NSString *)commentary
                            quantity:(NSDecimalNumber *)quantity
                          completion:(void (^)(NSError *error))completion;

- (void)asyncDeleteEntryWithID:(int64_t)entryID
                    completion:(void (^)(NSError *error))completion;

- (void)asyncEditEntryWithID:(int64_t)entryID
                   newItemID:(int64_t)itemID
               newCommentary:(NSString *)commentary
                 newQuantity:(NSDecimalNumber *)quantity
                   newStatus:(OrderEntryStatus)status
                  completion:(void (^)(NSError *))completion;
@end
