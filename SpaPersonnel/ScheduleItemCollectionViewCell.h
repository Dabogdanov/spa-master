//
//  ScheduleItemCollectionViewCell.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/28/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ConfirmedOrderEntry.h"

@interface ScheduleItemCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

// TODO: move to controller, view shouldn't know of OrderEntryStatus
- (void)setBackgroundColorWithStatus:(OrderEntryStatus)status;

@end
