//
//  CustomerManager.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/18/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Customer.h"
#import "ServerManager.h"

@interface CustomerManager : NSObject
- (instancetype)initWithCustomer:(Customer *)aCustomer serviceManager:(ServerManager *)aServerManager;

- (void)asyncGetCommentaryStringWithCompletion:(void (^)(NSString *, NSError *))completion;

- (void)asyncAppendCommentaryString:(NSString *)comment
                        withOrderID:(int64_t)orderID
                         completion:(void (^)(NSString *, NSError *))completion;

@end
