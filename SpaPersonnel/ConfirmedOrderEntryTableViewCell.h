//
//  ConfirmedOrderEntryTableViewCell.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/3/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

typedef NS_ENUM(NSInteger, ConfirmedOrderControlsState) {
  ConfirmedOrderControlsStateCancelled = 0,
  ConfirmedOrderControlsStateUserCanStart,
  ConfirmedOrderControlsStateUserCanFinish,
  ConfirmedOrderControlsStateUserDidFinish,
  ConfirmedOrderControlsStateIsBeingProcessed,
  ConfirmedOrderControlsStateDisplayAssignedServant
};

@class ConfirmedOrderEntryTableViewCell;

@protocol ConfirmedOrderEntryTableViewCellDelegate <NSObject>
- (void)confirmedOrderEntryTableViewCellUserDidPressStartButton:(ConfirmedOrderEntryTableViewCell *)cell;
- (void)confirmedOrderEntryTableViewCellUserDidPressFinishButton:(ConfirmedOrderEntryTableViewCell *)cell;
- (void)confirmedOrderEntryTableViewCellUserDidPressCancelButton:(ConfirmedOrderEntryTableViewCell *)cell;
- (void)confirmedOrderEntryTableViewCell:(ConfirmedOrderEntryTableViewCell *)cell userDidPressInfoButton:(UIButton *)button;

@end

@interface ConfirmedOrderEntryTableViewCell : UITableViewCell {
  IBOutlet UIView *controlsView;
  IBOutlet UIView *cancelledView;
  IBOutlet UIView *finishedIndicatorView;
  IBOutlet UIView *servantNameView;
  IBOutlet UILabel *quantityLabel;
  IBOutletCollection(UIButton) NSArray *buttons;
  IBOutletCollection(UIButton) NSArray *infoButtons;
  IBOutletCollection(UILabel) NSArray *openedClosedDateIntervalLabels;
}
@property (weak, nonatomic) IBOutlet UILabel *catalogItemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *servantNameLabel;

@property (weak, nonatomic) id <ConfirmedOrderEntryTableViewCellDelegate> delegate;

- (void)setControlsState:(ConfirmedOrderControlsState)state;
- (void)setButtonsEnabled:(BOOL)enabled;
- (void)setInfoButtonEnabled:(BOOL)enabled;

- (void)setOpenedClosedDateIntervalString:(NSString *)openedClosedDateIntervalString;
- (void)setQuantityString:(NSString *)quantityString;

@end
