//
//  ScheduleViewController.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/26/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ScheduleViewController.h"
#import "ServerManager.h"
#import "ScheduleItem.h"
#import "ScheduleProvider.h"
#import "ScheduleItemCollectionViewCell.h"
#import "UIColor+SpaPersonnel.h"
#import "ScheduleBuilderAdapter.h"

static NSString * const kScheduleCellID = @"scheduleCell";

@interface ScheduleViewController ()

@end

@implementation ScheduleViewController {
  NSArray *entries;
  NSDateFormatter *dateFormatter;
  NSDateFormatter *dayDateFormatter;
  NSDate *selectedDate;
  NSCalendar *calendar;
  
  NSNumber *orderIDToReloadWith;
  
  BOOL isLoading;
  
  NSNumber *lastSelectedEntryID;
  BOOL shouldReloadCurrentItem;
  
  ScheduleBuilderAdapter *scheduleBuilder;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
  lastSelectedEntryID = nil;
  
  NSString *title = [NSString stringWithFormat:@"%@ (ID: %lld)", self.services.servant.fullName, self.services.servant.ID];
  self.navigationItem.title = title;
  selectedDate = [NSDate date];
  [self.delegate scheduleViewController:self didSelectDate:selectedDate];
  isLoading = YES;
  [self updateDisplayedDate:selectedDate forDayPickerView:self.dayPickerView];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (NSDate *)selectedDate {
  return selectedDate;
}

- (NSArray *)entries {
  return entries;
}

- (ScheduleItem *)selectedItem {
  ScheduleItem *selectedItem = nil;
  NSIndexPath *selectedItemIndexPath = [self.collectionView indexPathsForSelectedItems].firstObject;
  if (selectedItemIndexPath.length) {
    selectedItem = entries[selectedItemIndexPath.row];
  }
  return selectedItem;
}

- (void)updateWithScheduleItems:(NSArray *)scheduleItems selectedItem:(ScheduleItem *)itemToSelect shouldIndicateLoading:(BOOL)shouldIndicateLoading {
  isLoading = shouldIndicateLoading;
  entries = scheduleItems;
  [self.collectionView reloadData];
  if (itemToSelect) {
    NSUInteger idx = [scheduleItems indexOfObjectPassingTest:^BOOL(ScheduleItem *item, NSUInteger idx, BOOL *stop) {
      return (item.orderEntryID == itemToSelect.orderEntryID);
    }];
    if (idx != NSNotFound) {
      [self selectEntryAtPosition:idx];
    }
  }
}

#pragma mark UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return entries.count > 0 ? entries.count : 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  ScheduleItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kScheduleCellID forIndexPath:indexPath];
  if (entries.count) {
    ScheduleItem *entry = entries[indexPath.row];
    NSString *timeString = [NSString stringWithFormat:@"%@ - %@", [[self dateFormatter] stringFromDate:entry.startDate], [[self dateFormatter] stringFromDate:entry.endDate]];
    cell.timeLabel.text = timeString;
    cell.titleLabel.text = entry.customerName ? entry.customerName : @"Нет имени клиента";
    [cell setBackgroundColorWithStatus:entry.status];
  } else {
    cell.titleLabel.text = isLoading ? @"Загрузка..." : @"Нет данных";
    cell.timeLabel.text = nil;
    [cell setBackgroundColor:[UIColor inactiveScheduleItemColor]];
  }
  cell.selectedBackgroundView.layer.borderColor = [UIColor selectedScheduleItemBorderColor].CGColor;
  cell.selectedBackgroundView.layer.borderWidth = 2.0f;
  return cell;
}

#pragma mark - Private Methods



- (NSDateFormatter *)dateFormatter {
  if (!dateFormatter) {
    dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"HH:mm";
  }
  return dateFormatter;
}

- (NSDateFormatter *)dayDateFormatter {
  if (!dayDateFormatter) {
    dayDateFormatter = [NSDateFormatter new];
    dayDateFormatter.dateFormat = @"dd.MM.yyyy";
  }
  return dayDateFormatter;
}

- (NSCalendar *)dayCalculationCalendar {
  if (!calendar) {
    calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
  }
  return calendar;
}

- (void)updateDisplayedDate:(NSDate *)date forDayPickerView:(DayPickerView *)dayPickerView {
  dayPickerView.dateLabel.text = [[self dayDateFormatter] stringFromDate:date];
}

- (void)selectEntryAtPosition:(NSUInteger)position {
  NSIndexPath *correspondingIndexPath = [NSIndexPath indexPathForRow:position inSection:0];
  [self.collectionView selectItemAtIndexPath:correspondingIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionTop];
}

- (BOOL)selectentryWithEntryID:(NSNumber *)ID fromScheduleEntries:(NSArray *)scheduleEntries {
  if (!shouldReloadCurrentItem) {
    return NO;
  }
  shouldReloadCurrentItem = NO;
  if (!ID) {
    return NO;
  }
  BOOL shouldContinue = YES;
  for (NSUInteger ui = 0; (ui < scheduleEntries.count) && shouldContinue; ui++) {
    ScheduleItem *entry = scheduleEntries[ui];
    if (entry.orderEntryID == ID.longLongValue) {
      [self selectEntryAtPosition:ui];
      [self.delegate scheduleViewController:self didSelectScheduleItem:entry];
      shouldContinue = NO;
    }
  }
  orderIDToReloadWith = nil;
  return !shouldContinue;
}

#pragma mark - DayPickerViewDelegate

- (void)dayPickerViewUserDidPressNextDateButton:(DayPickerView *)view {
  lastSelectedEntryID = nil;
  
  NSDateComponents *comps = [[self dayCalculationCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:selectedDate];
  comps.day += 1;
  selectedDate = [[self dayCalculationCalendar] dateFromComponents:comps];
  [self.delegate scheduleViewController:self didSelectDate:selectedDate];
  isLoading = YES;
  [self updateDisplayedDate:selectedDate forDayPickerView:view];
}

- (void)dayPickerViewUserDidPressPreviousDateButton:(DayPickerView *)view {
  lastSelectedEntryID = nil;
  
  NSDateComponents *comps = [[self dayCalculationCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:selectedDate];
  comps.day -= 1;
  selectedDate = [[self dayCalculationCalendar] dateFromComponents:comps];
  [self.delegate scheduleViewController:self didSelectDate:selectedDate];
  isLoading = YES;
  [self updateDisplayedDate:selectedDate forDayPickerView:view];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  if (!entries.count) {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
  } else {
    ScheduleItem *entry = entries[indexPath.row];
    [self.delegate scheduleViewController:self didSelectScheduleItem:entry];
    lastSelectedEntryID = @(entry.orderEntryID);
  }
}

@end
