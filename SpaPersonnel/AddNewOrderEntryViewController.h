//
//  AddNewOrderEntryViewController.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/23/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServicesFactory.h"
#import "ConfirmedOrderEntry.h"

@class AddNewOrderEntryViewController;

@protocol AddNewOrderEntryViewControllerDelegate <NSObject>
- (void)addNewOrderEntryViewControllerUserWantsToDissmiss:(AddNewOrderEntryViewController *)controller;
- (void)addNewOrderEntryViewController:(AddNewOrderEntryViewController *)controller userDidModifyOrderEntry:(ConfirmedOrderEntry *)orderEntry;

@end

@interface AddNewOrderEntryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
  IBOutlet UITableView * __weak _tableView;
  IBOutlet UIButton * __weak cancelButton;
  IBOutlet UIButton * __weak confirmButton;
  IBOutlet UILabel * __weak titleLabel;
}
@property (strong, nonatomic) ServicesFactory *services;
@property (weak, nonatomic) id <AddNewOrderEntryViewControllerDelegate> delegate;

- (void)fillWithOrderEntry:(ConfirmedOrderEntry *)entry;

@end
