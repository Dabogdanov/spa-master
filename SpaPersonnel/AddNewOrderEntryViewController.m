//
//  AddNewOrderEntryViewController.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/23/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "AddNewOrderEntryViewController.h"
#import "EditOrderEntryCommentTableViewCell.h"
#import "SelectCatalogItemTableViewCell.h"
#import "CatalogItemsListViewController.h"
#import "SelectCatalogItemView.h"
#import "QuantityTableViewCell.h"
#import "Quantity.h"
#import "UIColor+SpaPersonnel.h"

static NSString * const CatalogItemCellID = @"catalogItem";
static NSString * const QuantityCellID = @"quantity";
static NSString * const CommentaryCellID = @"commentary";

static NSString * const CatalogEntitiesListSegueID = @"catalogItemsList";

static CGFloat const DefaultContentViewWidth = 600.0f;
static CGFloat const DefaultContentViewHeight = 265.0f;

@interface AddNewOrderEntryViewController () <UIAlertViewDelegate, UITextFieldDelegate, UITextViewDelegate, CatalogItemsListViewControllerDelegate, QuantityTableViewCellDelegate>
@property (copy, nonatomic) void (^onAlertConfirm)();

@end

@implementation AddNewOrderEntryViewController {
  NSArray *tableModel;
  ConfirmedOrderEntry *entry;
  NSMutableDictionary *calculationViews;
  EditOrderEntryCommentView *_commentCalculationView;
  SelectCatalogItemView *_catalogItemCalculationView;
  NSNumberFormatter *quantityFormatter;
  BOOL screenDisabled;
  UIAlertView *confirmStatusChangeAlertView;
  
  NSCharacterSet *notAllowedQuantityCharacterSet;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    calculationViews = [NSMutableDictionary new];
    self.preferredContentSize = CGSizeMake(DefaultContentViewWidth, DefaultContentViewHeight);

    NSMutableCharacterSet *anAllowedQuantityCharacterSet = [NSMutableCharacterSet decimalDigitCharacterSet];
    NSString *decimalSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleDecimalSeparator];
    [anAllowedQuantityCharacterSet addCharactersInString:[NSString stringWithFormat:@"%@", decimalSeparator]];
    notAllowedQuantityCharacterSet = [anAllowedQuantityCharacterSet invertedSet];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [cancelButton setTitleColor:[UIColor buttonDefaultRedColor] forState:UIControlStateNormal];
  [cancelButton setTitleColor:[UIColor buttonHighlightedRedColor] forState:UIControlStateHighlighted];
  [self registerCellNibs];
  _commentCalculationView = [self newCommentCalculationView];
  _catalogItemCalculationView = [self newCatalogItemCalculationView];
  tableModel = [self newTableModel];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  titleLabel.text = self.title;
}

- (void)registerCellNibs {
  UINib *quantityCellNib = [UINib nibWithNibName:@"QuantityTableViewCell" bundle:nil];
  [_tableView registerNib:quantityCellNib forCellReuseIdentifier:QuantityCellID];
}

- (NSArray *)newTableModel {
  return @[CatalogItemCellID, QuantityCellID, CommentaryCellID];
}

- (EditOrderEntryCommentView *)newCommentCalculationView {
  NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"EditOrderEntryCommentView" owner:self options:nil];
  EditOrderEntryCommentView *view = (EditOrderEntryCommentView *)views.firstObject;
  return view;
}

- (SelectCatalogItemView *)newCatalogItemCalculationView {
  NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"SelectCatalogItemView" owner:self options:nil];
  SelectCatalogItemView *view = (SelectCatalogItemView *)views.firstObject;
  return view;
}

- (void)fillWithOrderEntry:(ConfirmedOrderEntry *)anEntry {
  entry = anEntry;
  [_tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:CatalogEntitiesListSegueID]) {
    CatalogItemsListViewController *catalogController = segue.destinationViewController;
    catalogController.services = self.services;
    catalogController.delegate = self;
  }
}

#pragma mark - Actions

- (IBAction)userDidPressCancelButton:(id)sender {
  [self.delegate addNewOrderEntryViewControllerUserWantsToDissmiss:self];
}

- (IBAction)userDidPressConfirmButton:(id)sender {
  [self displayConfirmationAlertWithConfirmationHandler:^{
    [self disableScreen];
    [self.delegate addNewOrderEntryViewController:self userDidModifyOrderEntry:entry];
  }];
}

- (void)disableScreen {
  cancelButton.enabled = NO;
  confirmButton.enabled = NO;
  [_tableView reloadData];
  screenDisabled = YES;
}

- (void)setEntryQuantity:(Quantity *)quantity {
  entry.quantity = [quantity decimalValue];
  BOOL quantityIsValid = (_Bool)quantity;
  confirmButton.enabled = quantityIsValid;
  NSUInteger idx = [tableModel indexOfObject:QuantityCellID];
  if (idx != NSNotFound) {
    QuantityTableViewCell *quantityCell = (QuantityTableViewCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
    [quantityCell setQuantityIsValid:quantityIsValid];
  }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return tableModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  NSString *reuseID = tableModel[indexPath.row];
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableModel[indexPath.row]];
  if ([reuseID isEqualToString:CommentaryCellID]) {
    cell = [self commentCellForTableView:tableView];
  }
  if ([reuseID isEqualToString:CatalogItemCellID]) {
    cell = [self selectCatalogItemCellForTableView:tableView];
  }
  if ([reuseID isEqualToString:QuantityCellID]) {
    cell = [self quantityCellForTableView:tableView];
  }
  NSAssert(cell != nil, @"[%@] No cell registered for cell reuse identifier: %@", [self class], tableModel[indexPath.row]);
  return cell;
}

- (UITableViewCell *)commentCellForTableView:(UITableView *)tableView {
  EditOrderEntryCommentTableViewCell *commentCell = [tableView dequeueReusableCellWithIdentifier:CommentaryCellID];
  if (!commentCell) {
    commentCell = [[EditOrderEntryCommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CommentaryCellID];
  }
  [commentCell setComment:entry.comment];
  [commentCell setTextViewDelegate:self];
  commentCell.selectionStyle = UITableViewCellSelectionStyleNone;
  calculationViews[CommentaryCellID] = commentCell.theContentView;
  return commentCell;
}

- (UITableViewCell *)selectCatalogItemCellForTableView:(UITableView *)tableView {
  SelectCatalogItemTableViewCell *catalogItemCell = [tableView dequeueReusableCellWithIdentifier:CatalogItemCellID];
  if (!catalogItemCell) {
    catalogItemCell = [[SelectCatalogItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CatalogItemCellID];
  }
  [catalogItemCell setItemName:entry.item.name];
  catalogItemCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
  catalogItemCell.backgroundColor = [UIColor mainScreenBackgroundColor];
  calculationViews[CatalogItemCellID] = catalogItemCell.theContentView;
  return catalogItemCell;
}

- (UITableViewCell *)quantityCellForTableView:(UITableView *)tableView {
  QuantityTableViewCell *quantityCell = [tableView dequeueReusableCellWithIdentifier:QuantityCellID];
  
  // TODO: quantity cell should actually display user set value, instead of entry.quantity
  // if text field has invalid number than entry.quantity is nil. If cell gets reloaded in this state, value in text field will be lost
  quantityCell.quantityTextField.text = [[self quantityFormatter] stringFromNumber:entry.quantity];
  
  quantityCell.delegate = self;
  quantityCell.selectionStyle = UITableViewCellSelectionStyleNone;
  [quantityCell setCellEnabled:!screenDisabled];
  [quantityCell setQuantityTextFieldDelegate:self];
  [quantityCell setQuantityIsValid:(_Bool)entry.quantity];
  return quantityCell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  CGFloat height = 0.0f;
  
  NSString *reuseID = tableModel[indexPath.row];
  if ([reuseID isEqualToString:CommentaryCellID]) {
    height = [self heightForCommentaryRow] + 1.0f; // 1.0f for separator
  } else if ([reuseID isEqualToString:CatalogItemCellID]) {
    height = [self heightForCatalogItemRow] + 1.0f; // 1.0f for separator
  }
  return MIN(MAX(60.0f, height), 2009.0f);
}

- (CGFloat)heightForCommentaryRow {
  EditOrderEntryCommentView *contentView = calculationViews[CommentaryCellID];
  CGFloat viewWidth = contentView.frame.size.width;
  if (![contentView comment]) {
    [_commentCalculationView setComment:entry.comment];
    contentView = _commentCalculationView;
    viewWidth = CGRectGetWidth(_tableView.frame);
  }
  CGSize desiredSize = [contentView sizeThatFits:CGSizeMake(viewWidth, CGFLOAT_MAX)];
  return desiredSize.height;
}

- (CGFloat)heightForCatalogItemRow {
  SelectCatalogItemView *contentView = calculationViews[CatalogItemCellID];
  CGFloat viewWidth = contentView.frame.size.width;
  if (![contentView itemName]) {
    [_catalogItemCalculationView setItemName:entry.item.name];
    contentView = _catalogItemCalculationView;
    viewWidth = CGRectGetWidth(_tableView.frame);
  }
  CGSize desiredSize = [contentView sizeThatFits:CGSizeMake(viewWidth, CGFLOAT_MAX)];
  return desiredSize.height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  if (screenDisabled) {
    return;
  }
  NSString *reuseID = tableModel[indexPath.row];
  if ([reuseID isEqualToString:CatalogItemCellID]) {
    [self performSegueWithIdentifier:CatalogEntitiesListSegueID sender:nil];
  }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  BOOL shouldChange = !screenDisabled;
  if (shouldChange) {
    NSRange notAllowedCharacterRange = [string rangeOfCharacterFromSet:notAllowedQuantityCharacterSet options:0];
    shouldChange = (notAllowedCharacterRange.location == NSNotFound);
    if (shouldChange) {
      NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
      NSDecimalNumber *newDecimalNumber = [self scanDecimalNumberFromString:newString];
      Quantity *newQuantity = [Quantity quantityWithDecimalNumber:newDecimalNumber];
      [self setEntryQuantity:newQuantity];
    }
  }
  return shouldChange;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
  return !screenDisabled;
}

- (void)textViewDidChange:(UITextView *)textView {
  entry.comment = textView.text;
  [_tableView beginUpdates];
  [_tableView endUpdates];
}

#pragma mark - CatalogItemsListViewControllerDelegate

- (void)catalogItemsListViewController:(CatalogItemsListViewController *)CatalogItemsListViewController userDidPickCatalogEntity:(CatalogEntity *)entity {
  entry.item = entity;
  entry.itemID = entity.ID; // TODO: refactor
  [_tableView reloadData];
  [self.navigationController popToViewController:self animated:YES];
}

- (void)catalogItemsListViewControllerUserWantsToDissmiss:(CatalogItemsListViewController *)CatalogItemsListViewController {
  [self.navigationController popToViewController:self animated:YES];
}

#pragma mark - QuantityTableViewCellDelegate

- (void)quantityTableViewCellUserDidPressAddButton:(QuantityTableViewCell *)cell {
  [self changeQuantityRepresentedInCell:cell byAddingOrSubtractingOne:YES];
}

- (void)quantityTableViewCellUserDidPressSubtractButton:(QuantityTableViewCell *)cell {
  [self changeQuantityRepresentedInCell:cell byAddingOrSubtractingOne:NO];
}

- (void)changeQuantityRepresentedInCell:(QuantityTableViewCell *)cell byAddingOrSubtractingOne:(BOOL)adding {
  NSString *text = cell.quantityTextField.text;
  
  if (!text.length) {
    Quantity *newQuantity = [Quantity defaultQuantity];
    cell.quantityTextField.text = [[self quantityFormatter] stringFromNumber:[newQuantity decimalValue]];
    
    [self setEntryQuantity:newQuantity];
  } else {
    NSDecimalNumber *oldDecimal = [self scanDecimalNumberFromString:cell.quantityTextField.text];
    Quantity *oldQuantity = [Quantity quantityWithDecimalNumber:oldDecimal];
    if (oldQuantity) {
      Quantity *newQuantity = adding ? [oldQuantity quantityByAddingOne] : [oldQuantity quantityBySubtractingOne];
      if (newQuantity) {
        cell.quantityTextField.text = [[self quantityFormatter] stringFromNumber:[newQuantity decimalValue]];
        
        [self setEntryQuantity:newQuantity];
      }
    }
  }
}

#pragma mark - UIAlertViewDelegate

- (void)displayConfirmationAlertWithConfirmationHandler:(void (^)())confirmHandler {
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Вы уверены?" message:nil delegate:self cancelButtonTitle:@"Нет" otherButtonTitles:@"Да", nil];
  confirmStatusChangeAlertView = alertView;
  
  AddNewOrderEntryViewController * __weak safeSelfReference = self;
  self.onAlertConfirm = ^{
    confirmHandler();
    safeSelfReference.onAlertConfirm = nil;
  };
  [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if ([confirmStatusChangeAlertView isEqual:alertView]) {
    if (buttonIndex != [alertView cancelButtonIndex]) {
      self.onAlertConfirm();
      confirmStatusChangeAlertView = nil;
    }
  }
}

#pragma mark - Formatters

- (NSDecimalNumber *)scanDecimalNumberFromString:(NSString *)string {
  NSDecimalNumber *decimalNumber = nil;
  NSScanner *stringScanner = [NSScanner localizedScannerWithString:string];
  NSDecimal decimal;
  if ([stringScanner scanDecimal:&decimal]) {
    decimalNumber = [NSDecimalNumber decimalNumberWithDecimal:decimal];
  }
  return decimalNumber;
}

- (NSNumberFormatter *)quantityFormatter {
  if (!quantityFormatter) {
    quantityFormatter = [[NSNumberFormatter alloc] init];
    quantityFormatter.locale = [NSLocale currentLocale];
    quantityFormatter.maximumFractionDigits = [Quantity numberOfDigitsAfterDecimalPoint];
    quantityFormatter.minimumIntegerDigits = 1;
  }
  return quantityFormatter;
}

@end
