//
//  Quantity.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/25/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface Quantity : NSObject <NSCopying>

+ (instancetype)defaultQuantity;
+ (instancetype)quantityWithDecimalNumber:(NSDecimalNumber *)decimalNumber;
+ (NSDecimalNumber *)lowestPossibleValue;
+ (NSDecimalNumber *)highestPossibleValue;
+ (NSUInteger)numberOfDigitsAfterDecimalPoint;

- (NSDecimalNumber *)decimalValue;
- (NSString *)description;

- (Quantity *)quantityByAddingOne;
- (Quantity *)quantityBySubtractingOne;

@end
