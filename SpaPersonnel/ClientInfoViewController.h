//
//  ClientInfoViewController.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/18/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServicesFactory.h"
#import "Customer.h"
#import "SpaModalPresentationController.h"

@class ClientInfoViewController;

@protocol ClientInfoViewControllerDelegate <NSObject>
- (void)clientInfoViewControllerUserWantsToDismiss:(ClientInfoViewController *)controller;

@end

@interface ClientInfoViewController : UIViewController <UITextViewDelegate, PresentedViewOffsetDataSource> {
  IBOutlet UIWebView * __weak webView;
  IBOutletCollection(UIView) NSArray *newCommentViews;
  IBOutlet UIButton * __weak addNewCommentButton;
  IBOutlet UIButton * __weak confirmButton;
  IBOutlet UIButton * __weak cancelButton;
  IBOutlet NSLayoutConstraint *actionsViewHeightConstraint;
  IBOutlet UITextView * __weak commentTextView;
  IBOutlet UILabel * __weak clientNameLabel;
  IBOutlet UIView *contentView;
}
@property (weak, nonatomic) id <ClientInfoViewControllerDelegate> delegate;
@property (strong, nonatomic) ServicesFactory *services;

- (void)fillWithCustomer:(Customer *)customer orderID:(int64_t)orderID;

@end
