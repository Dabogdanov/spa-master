//
//  DeviceIDProvider.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@class DeviceIDService;

@protocol DeviceIDServiceDelegate <NSObject>
- (void)deviceIDService:(DeviceIDService *)provider didReceiveNewDeviceID:(NSString *)aDeviceID;
- (void)deviceIDService:(DeviceIDService *)provider didFailWithError:(NSError *)error;

@end

@interface DeviceIDService : NSObject
@property (weak, nonatomic) id <DeviceIDServiceDelegate> delegate;

+ (NSString *)currentDeviceIDString;

/*
 * Starts attempts to get a device ID. 
 * DeviceIDProvider object stops itself as soon as it receives a device ID or an error occurs.
 */
- (void)start;
- (void)stop;

@end
