//
//  EditOrderEntryCommentTableViewCell.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/23/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "EditOrderEntryCommentTableViewCell.h"
#import "EditOrderEntryCommentView.h"

@implementation EditOrderEntryCommentTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
  if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"EditOrderEntryCommentView" owner:self options:nil];
    EditOrderEntryCommentView *view = (EditOrderEntryCommentView *)views.firstObject;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:view];
    
    self.theContentView = view;
    
    NSArray *constrs = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[view]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)];
    [self.contentView addConstraints:constrs];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)]];
  }
  return self;
}

- (void)setComment:(NSString *)comment {
  [self.theContentView setComment:comment];
}

- (void)setTextViewDelegate:(id<UITextViewDelegate>)delegate {
  [self.theContentView setTextViewDelegate:delegate];
}

@end
