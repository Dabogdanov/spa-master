//
//  CatalogCache.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/2/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "CatalogEntity.h"
#import "CatalogEntitiesList.h"

@interface CatalogCache : NSObject

- (void)asyncGetVersion:(void (^)(NSString *version, NSError *error))completion;
- (void)asyncGetCatalogEntityForID:(int64_t)orderID completion:(void (^)(CatalogEntity *entity, NSError *error))completion;
- (void)asyncGetCatalogEntitiesWithCompletion:(void (^)(NSArray *entities, NSError *error))completion;
- (void)asyncUpdateCacheWithEnitiesList:(CatalogEntitiesList *)entitiesList completion:(void (^)())completion;

@end
