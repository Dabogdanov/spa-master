//
//  ScheduleProvider.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ScheduleProvider.h"
#import "ScheduleItem.h"
#import "OrderEntriesArrayBuilder.h"
#import "OrderEntriesProvider.h"
#import "ConfirmedOrderEntry.h"
#import "ScheduleBuilder.h"

@interface ScheduleProvider () <OrderEntriesArrayBuilderDelegate, ScheduleBuilderDelegate>

@end

@implementation ScheduleProvider {
  NSMutableDictionary *entriesBuilders;
  NSMutableDictionary *dateComponents;
  ServerManager *serverManager;
}

- (instancetype)initWithServerManager:(ServerManager *)aServerManager {
  if (self = [super init]) {
    serverManager = aServerManager;
    entriesBuilders = [NSMutableDictionary new];
    dateComponents = [NSMutableDictionary new];
  }
  return self;
}

- (void)createScheduleForDate:(NSDate *)date {
  OrderEntriesProvider *entriesProvider = [[OrderEntriesProvider alloc] initWithServerManager:serverManager date:date];
  [self storeBuilder:entriesProvider withDate:date];
  [entriesProvider startWithSuccess:^(NSArray *orderEntries) {
    [self orderEntriesProvider:entriesProvider didBuildNewEntriesArray:orderEntries];
  } failure:^(NSError *error) {
    [self orderEntriesProvider:entriesProvider didFailToBuildEntriesArray:error];
  }];
}

- (void)orderEntriesProvider:(OrderEntriesProvider *)provider didBuildNewEntriesArray:(NSArray *)entries {
  NSDate *date = [self storedDateForBuilder:provider];
  if (!date) { // this means that we've created a newer builder
    return;
  }
  [self removeBuilderForDate:date];
  
  ScheduleBuilder *scheduleBuilder = [[ScheduleBuilder alloc] initWithOrderEntries:entries servant:serverManager.session.servant];
  [self storeBuilder:scheduleBuilder withDate:date];
  
  scheduleBuilder.delegate = self;
  [scheduleBuilder build];
}

- (void)orderEntriesProvider:(OrderEntriesProvider *)provider didFailToBuildEntriesArray:(NSError *)error {
  NSLog(@"Error building entries array: %@", error.localizedDescription);
}

#pragma mark - OrderEntriesArrayBuilderDelegate

- (void)orderEntriesArrayBuilder:(OrderEntriesArrayBuilder *)builder didBuildNewEntriesArray:(NSArray *)entries {
  NSDate *date = [self storedDateForBuilder:builder];
  if (!date) { // this means that we've created a newer builder
    return;
  }
  [self removeBuilderForDate:date];

  ScheduleBuilder *scheduleBuilder = [[ScheduleBuilder alloc] initWithOrderEntries:entries servant:serverManager.session.servant];
  [self storeBuilder:scheduleBuilder withDate:date];
  
  scheduleBuilder.delegate = self;
  [scheduleBuilder build];
}

- (void)orderEntriesArrayBuilder:(OrderEntriesArrayBuilder *)builder didFailToBuildEntriesArray:(NSError *)error {
  NSDate *date = [self storedDateForBuilder:builder];
  if (!date) { // this means that we've created a newer builder
    return;
  }
  [self removeBuilderForDate:date];
  [self.delegate scheduleProvider:self didFailWithError:error];
}

#pragma mark - ScheduleBuilderDelegate

- (void)scheduleBuilder:(ScheduleBuilder *)aScheduleBuilder didBuildSchedule:(NSArray *)schedule {
  NSDate *date = [self storedDateForBuilder:aScheduleBuilder];
  if (!date) { // this means that we've created a newer builder
    return;
  }
  [self removeBuilderForDate:date];
  
  [self.delegate scheduleProvider:self didCreateSchedule:schedule forDate:date];
}

#pragma mark - Builders Ownership

- (void)storeBuilder:(id)builder withDate:(NSDate *)date {
  NSValue *builderPointer = [NSValue valueWithPointer:(__bridge void *)builder];
  [dateComponents setObject:[date copy] forKey:builderPointer];
  
  [entriesBuilders setObject:builder forKey:date];
}

- (void)removeBuilderForDate:(NSDate *)date {
  id builder = [entriesBuilders objectForKey:date];
  NSValue *builderPointer = [NSValue valueWithPointer:(__bridge void *)builder];
  [dateComponents removeObjectForKey:builderPointer];
  [entriesBuilders removeObjectForKey:date];
}

- (NSDate *)storedDateForBuilder:(id)builder {
  NSValue *builderPointer = [NSValue valueWithPointer:(__bridge void *)builder];
  NSDate *date = [dateComponents objectForKey:builderPointer];
  return date;
}

@end
