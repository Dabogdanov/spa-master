//
//  MainViewController.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/27/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServicesFactory.h"
#import "ScheduleViewController.h"

@class MainViewController;

@protocol MainViewControllerDelegate <NSObject>
- (void)mainViewControllerUserWantsToDismiss:(MainViewController *)mainViewController;

@end

@interface MainViewController : UIViewController {
  IBOutlet UILabel *shopNameLabel;
  IBOutlet UILabel *servantNameLabel;
  IBOutlet UIBarButtonItem *exitButton;
}
@property (weak, nonatomic) id <MainViewControllerDelegate> delegate;
@property (strong, nonatomic) ServicesFactory *services;
@property (strong, nonatomic) IBOutlet UIImageView *mainIconView;
@end
