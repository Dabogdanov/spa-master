//
//  OrderEntriesProvider.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/23/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "OrderEntriesProvider.h"
#import "OrderEntriesToCatalogEntitiesMatcher.h"
#import "CatalogCacheManager.h"

@implementation OrderEntriesProvider {
  ServerManager *serverManager;
  NSDate *date;
  OrderEntriesToCatalogEntitiesMatcher *matcher;
}

- (instancetype)initWithServerManager:(ServerManager *)aServerManager date:(NSDate *)aDate {
  if (self = [super init]) {
    serverManager = aServerManager;
    date = aDate;
  }
  return self;
}

- (void)startWithSuccess:(void (^)(NSArray *))success failure:(void (^)(NSError *))failure {
  [serverManager fetchOrderEntriesForDate:date
                                  success:^(NSArray *orderEntries) {
                                    [self handleEntriesResponse:orderEntries successBlock:success];
                                  }
                                  failure:^(NSError *error) {
                                    [self handleFailureWithError:error failureBlock:failure];
                                  }];
}

- (void)handleEntriesResponse:(NSArray *)entries successBlock:(void (^)(NSArray *orderEntries))success {
  CatalogCacheManager *cacheManager = [[CatalogCacheManager alloc] initWithServerManager:serverManager];
  matcher = [[OrderEntriesToCatalogEntitiesMatcher alloc] initWithCatalogCacheManager:cacheManager];
  [matcher matchConfirmedOrderEntries:entries withCompletion:^(NSError *error) {
    matcher = nil;
    success(entries);
  }];
}

- (void)handleFailureWithError:(NSError *)error failureBlock:(void (^)(NSError *error))failure {
  failure(error);
}

@end
