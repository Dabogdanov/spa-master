//
//  ScheduleBuilder.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/1/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Servant.h"

@class ScheduleBuilder;

@protocol ScheduleBuilderDelegate <NSObject>
- (void)scheduleBuilder:(ScheduleBuilder *)scheduleBuilder didBuildSchedule:(NSArray *)schedule;

@end

/*
 * ScheduleBuilder constructs array of ScheduleItem objects from given array of ConfirmedOrderEntry objects.
 * Also assigns each ScheduleItem an Order containing ConfirmedOrderEntry objects with same orderID from given order entries array.
 */

@interface ScheduleBuilder : NSObject
@property (weak, nonatomic) id <ScheduleBuilderDelegate> delegate;
- (instancetype)initWithOrderEntries:(NSArray *)anEntriesArr servant:(Servant *)aServant;
- (void)build;

@end
