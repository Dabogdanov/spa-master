//
//  ScheduleProviderAdapter.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ScheduleProviderAdapter.h"

@interface ScheduleProviderAdapter () <ScheduleProviderDelegate>

@end

@implementation ScheduleProviderAdapter {
  ScheduleProviderCompletion completionBlock;
  NSDate *date;
  ScheduleProvider *provider;
}

+ (ScheduleProviderAdapter *)asyncGetScheduleWithDate:(NSDate *)date completionBlock:(ScheduleProviderCompletion)completion servicesFactory:(ServicesFactory *)factory {
  ScheduleProviderAdapter *adapter = [[ScheduleProviderAdapter alloc] initWithDate:date completionBlock:completion servicesFactory:factory];
  [adapter start];
  return adapter;
}

- (instancetype)initWithDate:(NSDate *)aDate completionBlock:(ScheduleProviderCompletion)completion servicesFactory:(ServicesFactory *)factory {
  if (self = [super init]) {
    date = aDate;
    completionBlock = [completion copy];
    provider = [factory newScheduleProvider];
    provider.delegate = self;
  }
  return self;
}

- (void)start {
  [provider createScheduleForDate:date];
}

#pragma mark - ScheduleProviderDelegate

- (void)scheduleProvider:(ScheduleProvider *)provider didCreateSchedule:(NSArray *)schedule forDate:(NSDate *)aDate {
  completionBlock(schedule, aDate, nil);
}

- (void)scheduleProvider:(ScheduleProvider *)provider didFailWithError:(NSError *)error {
  completionBlock(nil, date, error);
}

@end
