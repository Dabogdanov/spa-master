//
//  CustomerManager.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/18/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "CustomerManager.h"

@implementation CustomerManager {
  Customer *customer;
  ServerManager *serverManager;
}

- (instancetype)initWithCustomer:(Customer *)aCustomer serviceManager:(ServerManager *)aServerManager {
  if (self = [super init]) {
    customer = aCustomer;
    serverManager = aServerManager;
  }
  return self;
}

- (void)asyncGetCommentaryStringWithCompletion:(void (^)(NSString *, NSError *))completion {
  [serverManager asyncGetInfoForCustomerWithID:customer.ID success:^(NSString *HTMLString) {
    completion (HTMLString, nil);
  } failure:^(NSError *error) {
    completion(nil, error);
  }];
}

- (void)asyncAppendCommentaryString:(NSString *)comment
                        withOrderID:(int64_t)orderID
                         completion:(void (^)(NSString *, NSError *))completion {
  [serverManager asyncAppendString:comment toInfoForCustomerWithID:customer.ID orderID:orderID success:^(NSString *HTMLString) {
    completion(HTMLString, nil);
  } failure:^(NSError *error) {
    completion(nil, error);
  }];
}

@end
