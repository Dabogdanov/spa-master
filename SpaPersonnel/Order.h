//
//  Order.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Customer.h"
#import "ConfirmedOrderEntry.h"

@interface Order : NSObject
@property (readonly, nonatomic) NSArray *orderEntries;
@property (copy, nonatomic) NSArray *recommendedEntries;
@property (strong, nonatomic) Customer *customer;
@property (assign, nonatomic) int64_t ID;
- (void)addEntry:(ConfirmedOrderEntry *)entry;
- (void)updateOldEntryWithNew:(ConfirmedOrderEntry *)newEntry;
- (void)removeEntryWithID:(int64_t)entryID;

@end
