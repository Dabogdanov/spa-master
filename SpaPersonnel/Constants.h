//
//  NSObject+Constants_h.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/25/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

extern NSString * const kBaseDefaultURL;
extern NSString * const kAPIVersion;
extern NSString * const kShopID;
extern NSString * const kAuthHeaderField;

//extern NSString * const kDeviceIDStub;
