//
//  SelectCatalogItemView.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface SelectCatalogItemView : UIView {
  IBOutlet UILabel * __weak itemNameLabel;
}

- (NSString *)itemName;
- (void)setItemName:(NSString *)itemName;

@end
