//
//  DoubleLabelSpaButton.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/4/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "SpaButton.h"

// is a hack
@interface DoubleLabelSpaButton : SpaButton
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;

@end
