//
//  DigitButton.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/18/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "DecimalDigit.h"

@interface DigitButton : UIButton
@property (strong, nonatomic) DecimalDigit *digit;

@end
