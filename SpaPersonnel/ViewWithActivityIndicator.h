//
//  ViewWithActivityIndicator.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/30/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface ViewWithActivityIndicator : UIView
- (void)lockInputAndShowActivityIndicator;
- (void)unlockInputAndHideActivityIndicator;

@end
