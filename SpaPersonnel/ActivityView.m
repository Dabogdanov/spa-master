//
//  ActivityView.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/29/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ActivityView.h"
#import "UIColor+SpaPersonnel.h"
#import "UIView+SpaPersonnel.h"

@implementation ActivityView

- (void)awakeFromNib {
  [activityIndicatorBackground makeCornersRound];
}

@end
