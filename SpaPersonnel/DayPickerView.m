//
//  DayPickerView.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/27/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "DayPickerView.h"

@implementation DayPickerView

- (IBAction)userDidPressButton:(id)sender {
  if ([sender isEqual:previousDateButton]) {
    [self.delegate dayPickerViewUserDidPressPreviousDateButton:self];
  } else if ([sender isEqual:nextDateButton]) {
    [self.delegate dayPickerViewUserDidPressNextDateButton:self];
  }
}

@end
