//
//  AuthViewController.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServicesFactory.h"
#import "DigitView.h"

@interface AuthViewController : UIViewController {
  IBOutletCollection(DigitView) NSArray *digitViews;
  IBOutlet UIButton *infoButton;
}

@property (strong, nonatomic) ServicesFactory *services;

@end
