//
//  SpaModalPresentationController.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/19/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "SpaModalPresentationController.h"
#import "UIColor+SpaPersonnel.h"
#import "UIView+SpaPersonnel.h"

static NSString * const kFrameOffsetKeyPath = @"frameOffset";

@implementation SpaModalPresentationController {
  UIView * __weak _dimmingView;
}

- (void)dealloc {
  [_offsetDataSource removeObserver:self
                         forKeyPath:kFrameOffsetKeyPath];
}

- (void)setOffsetDataSource:(NSObject<PresentedViewOffsetDataSource> *)offsetDataSource {
  [_offsetDataSource removeObserver:self
                         forKeyPath:kFrameOffsetKeyPath];
  _offsetDataSource = offsetDataSource;
  [_offsetDataSource addObserver:self
                      forKeyPath:kFrameOffsetKeyPath
                         options:NSKeyValueObservingOptionNew
                         context:nil];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
  if ([object isEqual:_offsetDataSource] && [keyPath isEqualToString:kFrameOffsetKeyPath]) {
    [self updateSize];
  }
}

- (void)updateSize {
  if (self.presentedView.window) {
    self.presentedView.frame = [self frameForContainedView];
  }
}

- (CGRect)frameForContainedView {
  CGSize desiredSize = [self.presentedViewController preferredContentSize];
  CGPoint desiredOffset = [self.offsetDataSource frameOffset];
  CGFloat xInset = (CGRectGetWidth(self.containerView.bounds) - desiredSize.width) / 2.f;
  CGFloat yInset = (CGRectGetHeight(self.containerView.bounds) - desiredSize.height) / 2.f;
  CGFloat xOrigin = MAX(0, CGRectGetMinX(self.containerView.bounds) + xInset + desiredOffset.x);
  CGFloat yOrigin = MAX(0, CGRectGetMinY(self.containerView.bounds) + yInset + desiredOffset.y);
  CGRect frame = CGRectMake(xOrigin,
                            yOrigin,
                            MIN(CGRectGetWidth(self.containerView.bounds) - 2 * xInset, CGRectGetMaxX(self.containerView.bounds) - xOrigin),
                            MIN(CGRectGetHeight(self.containerView.bounds) - 2 * yInset, CGRectGetMaxY(self.containerView.bounds) - yOrigin));
  
  return frame;
}

- (CGRect)frameOfPresentedViewInContainerView {
  return [self frameForContainedView];
}

- (void)presentationTransitionWillBegin {
  UIView *aDimmingView = _dimmingView;
  if (!aDimmingView) {
    aDimmingView = [[UIView alloc] initWithFrame:self.containerView.bounds];
    aDimmingView.backgroundColor = [UIColor colorWithRed255:171 green255:171 blue255:171 alpha:0.5];
    _dimmingView = aDimmingView;
  }
  
  [self.presentedView makeCornersRound];
  
  // Add a custom dimming view behind the presented view controller's view
  [[self containerView] addSubview:_dimmingView];
  [self.containerView addSubview:[[self presentedViewController] view]];
  
  // Use the transition coordinator to set up the animations.
  id <UIViewControllerTransitionCoordinator> transitionCoordinator =
  [[self presentingViewController] transitionCoordinator];
  
  // Fade in the dimming view during the transition.
  [_dimmingView setAlpha:0.0];
  [transitionCoordinator animateAlongsideTransition:
   ^(id<UIViewControllerTransitionCoordinatorContext> context) {
     [_dimmingView setAlpha:1.0];
   } completion:nil];
}

- (void)presentationTransitionDidEnd:(BOOL)completed {
  // Remove the dimming view if the presentation was aborted.
  if (!completed) {
    [_dimmingView removeFromSuperview];
  }
}

- (void)dismissalTransitionWillBegin {
  id <UIViewControllerTransitionCoordinator> transitionCoordinator = self.presentingViewController.transitionCoordinator;
  [transitionCoordinator animateAlongsideTransition:
   ^(id<UIViewControllerTransitionCoordinatorContext> context) {
     [_dimmingView setAlpha:0.0];
   } completion:nil];
}

- (void)dismissalTransitionDidEnd:(BOOL)completed {
  if (completed) {
    [_dimmingView removeFromSuperview];
  }
}

@end
