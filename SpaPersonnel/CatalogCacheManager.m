//
//  CatalogManager.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/2/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "CatalogCacheManager.h"
#import "ServerManager.h"
#import "CatalogCache.h"

@implementation CatalogCacheManager {
  ServerManager *serverManager;
}

- (CatalogCache *)catalogCache {
  static CatalogCache *cache;
  static dispatch_once_t token;
  dispatch_once(&token, ^{
    cache = [[CatalogCache alloc] init];
  });
  return cache;
}

- (instancetype)initWithServerManager:(ServerManager *)aServerManager {
  if (self = [super init]) {
    serverManager = aServerManager;
  }
  return self;
}

- (void)checkForUpdatesWithCompletion:(void (^)(BOOL success, NSError *error))completion {
  [[self catalogCache] asyncGetVersion:^(NSString *version, NSError *error) {
    [serverManager checkCatalogUpdatesWithVersion:version success:^(CatalogEntitiesList *entitiesList) {
      if (entitiesList) {
        [self handleCatalogUpdateResponseWithCatalogEntitiesList:entitiesList completion:[completion copy]];
      }
    } failure:^(NSError *error) {
      completion(NO, error);
    }];
  }];
}

- (void)handleCatalogUpdateResponseWithCatalogEntitiesList:(CatalogEntitiesList *)entitiesList completion:(void (^)(BOOL success, NSError *error))completion {
  if (entitiesList.catalogEntities.count) {
    [[self catalogCache] asyncUpdateCacheWithEnitiesList:entitiesList completion:^{
      completion(YES, nil);
    }];
  } else {
    completion(YES, nil);
  }
}

- (void)fetchCachedCatalogEntityWithID:(int64_t)itemID completion:(void (^)(CatalogEntity *, NSError *))completion {
  // try to fetch from memory, if memory is empty, method should update memory cache from Core Data
  [[self catalogCache] asyncGetCatalogEntityForID:itemID completion:[completion copy]];
}

- (void)fetchCachedCatalogEntitiesWithCompletion:(void (^)(NSArray *, NSError *))completion {
  [[self catalogCache] asyncGetCatalogEntitiesWithCompletion:^(NSArray *entities, NSError *error) {
    completion(entities, error);
  }];
}

@end
