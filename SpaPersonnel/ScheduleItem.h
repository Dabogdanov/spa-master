//
//  ScheduleItem.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/1/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Order.h"
#import "ConfirmedOrderEntry.h"

@interface ScheduleItem : NSObject
@property (strong, nonatomic) Order *order;
@property (copy, nonatomic) NSString *customerName;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (assign, nonatomic) OrderEntryStatus status;
@property (assign, nonatomic) int64_t orderEntryID;

@end
