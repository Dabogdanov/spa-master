//
//  DigitView.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/18/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "DecimalDigit.h"

@interface DigitView : UIView {
  IBOutlet UILabel *digitLabel;
  IBOutlet UIImageView *noDigitImageView;
}
@property (assign, nonatomic, setter=setActive:) BOOL isActive;

- (void)setDigit:(DecimalDigit *)digit;

- (void)clear;

@end
