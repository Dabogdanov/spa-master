//
//  NSString+SpaPersonnel.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/19/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "NSString+SpaPersonnel.h"

@implementation NSString (SpaPersonnel)

+ (NSString *)sexAndAgeStringForCustomer:(Customer *)customer {
  if (customer) {
    NSString *sex = (customer.sex == CustomerSexFemale) ? @"жен. " : @"муж. ";
    NSString *suffix = [self suffixForAge:customer.age];
    NSString *age = [NSString stringWithFormat:@"%ld %@", (long)customer.age, suffix];
    NSString *sexAndAgeString = [sex stringByAppendingString:age];
    return sexAndAgeString;
  }
  return nil;
}

// probably can be replaced with formatter, should be replaced if L10n ever happens.
+ (NSString *)suffixForAge:(NSInteger)age {
  NSInteger lastTwoDigits = age % 100;
  if ((lastTwoDigits >= 11) && (lastTwoDigits <= 14)) {
    return @"лет";
  }
  NSInteger lastDigit = age % 10;
  if (lastDigit == 1) {
    return @"год";
  } else if ((lastDigit >= 2) && (lastDigit <= 4)) {
    return @"года";
  } else {
    return @"лет";
  }
}

+ (NSString *)clietFullNameStringForCustomer:(Customer *)customer {
  return [@"Клиент: " stringByAppendingString:customer.fullName ? customer.fullName : @""];
}


@end
