//
//  AuthorizationManager.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "AuthorizationManager.h"
#import "DeviceIDService.h"
#import "Constants.h"

@interface AuthorizationManager () <DeviceIDServiceDelegate>
@end

@implementation AuthorizationManager {
  ServerManager *serverManager;
  DeviceIDService *deviceIDProvider;
  NSString *authKey;
  void (^completion)(Session *session, NSError *error);
}

- (instancetype)initWithServerManager:(ServerManager *)aServerManager {
  if (self = [super init])
  {
    serverManager = aServerManager;
    deviceIDProvider = [[DeviceIDService alloc] init];
    deviceIDProvider.delegate = self;
  }
  return self;
}

- (void)authWithKey:(NSString *)key
         completion:(void (^)(Session *session, NSError *error))aCompletion {
  completion = aCompletion;
  
  NSString *deviceID = [DeviceIDService currentDeviceIDString];
//  NSString *deviceID = kDeviceIDStub;
  
  if (!deviceID)
  {
    authKey = key;
    [deviceIDProvider start];
  } else {
    [self continueAuthWithPassword:key deviceID:deviceID];
  }
}

- (void)continueAuthWithPassword:(NSString *)password deviceID:(NSString *)deviceID {
   /* if(DEBUG){
        completion(nil, nil);
        return;
    }*/
  [serverManager authWithPassword:password deviceID:deviceID success:^(Session *session) {
    completion(session, nil);
  } failure:^(NSError *error) {
    completion(nil, error);
  }];
}

#pragma mark - DeviceIDProviderDelegate

- (void)deviceIDService:(DeviceIDService *)provider didReceiveNewDeviceID:(NSString *)aDeviceID {
  [self continueAuthWithPassword:authKey deviceID:aDeviceID];
  authKey = nil;
}

- (void)deviceIDService:(DeviceIDService *)provider didFailWithError:(NSError *)error {
  completion(nil, error);
}

@end
