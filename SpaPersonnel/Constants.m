//
//  Constants.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/25/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

NSString * const kBaseDefaultURL = @"http://dev2.api.4menu.ru/";
NSString * const kAPIVersion = @"v1";
NSString * const kShopID = @"002A2282-2667-4D63-A137-A4CF333AAD4D";
NSString * const kAuthHeaderField = @"X-Session";

//NSString * const kDeviceIDStub = @"35 442406 138130 1";

