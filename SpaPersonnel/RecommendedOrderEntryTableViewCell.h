//
//  RecommendedOrderEntryTableViewCell.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/3/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "RoundSpaButton.h"

@class CollapsableButton;
@class CollapsableLabel;
@class CollapsableView;
@class RecommendedOrderEntryTableViewCell;

@protocol RecommendedOrderEntryTableViewCellDelegate <NSObject>
- (void)recommendedOrderEntryTableViewCellUserDidPressCancelButton:(RecommendedOrderEntryTableViewCell *)cell;
- (void)recommendedOrderEntryTableViewCellUserDidPressEditButton:(RecommendedOrderEntryTableViewCell *)cell;
- (void)recommendedOrderEntryTableViewCell:(RecommendedOrderEntryTableViewCell *)cell userDidPressInfoButton:(UIButton *)button;

@end

@interface RecommendedOrderEntryTableViewCell : UITableViewCell {
  IBOutlet CollapsableButton *editButton;
  IBOutlet CollapsableButton *infoButton;
  IBOutlet CollapsableButton *cancelButton;
  IBOutlet UILabel *quantityLabel;
}

@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@property (weak, nonatomic) id <RecommendedOrderEntryTableViewCellDelegate> delegate;

- (void)setCancelOn;
- (void)setCancelOff;

- (void)setQuantity:(NSDecimalNumber *)quantity;

- (void)setInfoOn;
- (void)setInfoOff;

- (void)setEditOn;
- (void)setEditOff;

- (void)setButtonsEnabled:(BOOL)enabled;

@end

// TODO: get rid of copy paste if possible

@interface CollapsableLabel : UILabel {
  IBOutlet NSLayoutConstraint *widthConstraint;
  IBOutlet NSLayoutConstraint *paddingConstraint;
}

- (void)collapse;
- (void)expand;

@end

@interface CollapsableButton : RoundSpaButton {
  IBOutlet NSLayoutConstraint *widthConstraint;
  IBOutlet NSLayoutConstraint *paddingConstraint;
}

- (void)collapse;
- (void)expand;

@end

@interface CollapsableView : UIView {
  IBOutlet NSLayoutConstraint *widthConstraint;
  IBOutlet NSLayoutConstraint *paddingConstraint;
}

- (void)collapse;
- (void)expand;

@end