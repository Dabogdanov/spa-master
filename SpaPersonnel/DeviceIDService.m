//
//  DeviceIDProvider.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "DeviceIDService.h"
#import "Constants.h"
#import "KeychainItemWrapper.h"

static NSTimeInterval const kRetryDelay = 1.0;

@implementation DeviceIDService

static NSString *CurrentDeviceID;

+ (NSString *)currentDeviceIDString {
  return CurrentDeviceID;
}

- (void)start {
  NSString *deviceID = [self loadDeviceIDFromKeychain];
  
  if (deviceID.length) {
    [self stopWithDeviceID:deviceID];
  } else {
    [self requestDeviceID];
  }
}

- (void)stop {
  [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

#pragma mark - Private methods

- (void)requestDeviceID {
  NSUUID *deviceUUID = [[UIDevice currentDevice] identifierForVendor];
  if (deviceUUID) {
    NSString *deviceID = deviceUUID.UUIDString;
    [self saveDeviceUUIDToKeychain:deviceID];
    [self stopWithDeviceID:deviceID];
  } else {
    [self performSelector:@selector(requestDeviceID) withObject:nil afterDelay:kRetryDelay];
  }
}

- (void)stopWithDeviceID:(NSString *)deviceID {
  [self stop];
  [DeviceIDService setCurrentDeviceID:deviceID];
  [self.delegate deviceIDService:self didReceiveNewDeviceID:[DeviceIDService currentDeviceIDString]];
}

#pragma mark Device ID Cache

+ (void)setCurrentDeviceID:(NSString *)deviceUUID {
  CurrentDeviceID = deviceUUID;
}

#pragma mark Keychain access

- (void)saveDeviceUUIDToKeychain:(NSString *)deviceID {
  // since accessGroup:nil only this app can access deviceID in keychain
  KeychainItemWrapper *keychainWrapper = [[KeychainItemWrapper alloc] initWithIdentifier:[self keychainItemID]
                                                                             accessGroup:nil];
  [keychainWrapper setObject:deviceID forKey:(__bridge id)kSecAttrAccount];
}

- (NSString *)loadDeviceIDFromKeychain {
  KeychainItemWrapper *keychainWrapper = [[KeychainItemWrapper alloc] initWithIdentifier:[self keychainItemID]
                                                                             accessGroup:nil];
  NSString *deviceId = [keychainWrapper objectForKey:(__bridge id)kSecAttrAccount];
  return deviceId;
}

- (NSString *)keychainItemID {
  return [NSString stringWithFormat:@"%@.deviceid", [[NSBundle mainBundle] bundleIdentifier]];
}

@end
