//
//  UIAlertView+SpaPersonnel.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/16/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "UIAlertView+SpaPersonnel.h"

@implementation UIAlertView (SpaPersonnel)

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                  message:message
                                                 delegate:nil
                                        cancelButtonTitle:@"ОК"
                                        otherButtonTitles:nil];
  [alert show];
}

@end
