//
//  QuantityTableViewCell.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/23/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "QuantityTableViewCell.h"
#import "UIColor+SpaPersonnel.h"

@implementation QuantityTableViewCell

- (void)setQuantityTextFieldDelegate:(id <UITextFieldDelegate>)quantityTextFieldDelegate {
  self.quantityTextField.delegate = quantityTextFieldDelegate;
}

- (id <UITextFieldDelegate>)quantityTextFieldDelegate {
  return self.quantityTextField.delegate;
}

- (void)setCellEnabled:(BOOL)enabled {
  addButton.enabled = enabled;
  subtractButton.enabled = enabled;
}

- (void)setDisabledReasonString:(NSString *)string {
  diasbledReasonLabel.text = string;
}

- (void)setQuantityIsValid:(BOOL)valid {
  self.quantityTextField.backgroundColor = !valid ? [UIColor buttonHighlightedRedColor] : [[UITextView appearance] backgroundColor];
}

#pragma mark - Actions

- (IBAction)userDidPressSubtractButton:(id)sender {
  [self.delegate quantityTableViewCellUserDidPressSubtractButton:self];
}

- (IBAction)userDidPressAddButton:(id)sender {
  [self.delegate quantityTableViewCellUserDidPressAddButton:self];
}

@end
