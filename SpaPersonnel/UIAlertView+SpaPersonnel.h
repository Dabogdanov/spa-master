//
//  UIAlertView+SpaPersonnel.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/16/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface UIAlertView (SpaPersonnel)
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;

@end
