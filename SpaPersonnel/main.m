//
//  main.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/21/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "AppDelegate.h"
#import "SpaPersonnelApplication.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, NSStringFromClass([SpaPersonnelApplication class]), NSStringFromClass([AppDelegate class]));
  }
}
