//
//  CommentaryViewController.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/26/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface CommentaryViewController : UIViewController {
  IBOutlet UILabel *commentaryLabel;
}

- (void)fillWithCommentary:(NSString *)commentary;

@end
