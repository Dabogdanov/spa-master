//
//  OrderViewController.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/27/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServicesFactory.h"
#import "ScheduleItem.h"

@class OrderViewController;

@protocol OrderViewControllerDelegate <NSObject>
- (void)orderViewController:(OrderViewController *)controller didChangeScheduleItem:(ScheduleItem *)anItem;
- (void)orderViewController:(OrderViewController *)controller requestsUpdateWithScheduleItem:(ScheduleItem *)anItem;

@end

@interface OrderViewController : UIViewController {
  IBOutlet UILabel *clientNameLabel;
  IBOutlet UILabel *clientSexAndAgeLabel;
  IBOutlet UIButton *clientInfoButton;
}
@property (strong, nonatomic) ServicesFactory *services;

@property (weak, nonatomic) id <OrderViewControllerDelegate> delegate;

- (void)fillWithScheduleItem:(ScheduleItem *)scheduleItem;

@end

@protocol OrderTableViewControllerDelegate <NSObject>
- (void)orderViewControllerDidChangeOrder:(UIViewController *)controller;
- (void)orderViewControllerDidMadeOrderChangeRequiringFullUpdate:(UIViewController *)controller;

@end