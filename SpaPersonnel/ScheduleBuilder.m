//
//  ScheduleBuilder.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/1/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ScheduleBuilder.h"
#import "ConfirmedOrderEntry.h"
#import "ScheduleItem.h"

@implementation ScheduleBuilder {
  NSArray *entries;
  NSMutableSet *orders;
  Servant *servant;
}

- (instancetype)initWithOrderEntries:(NSArray *)anEntriesArr servant:(Servant *)aServant {
  if (self = [super init]) {
    entries = anEntriesArr;
    orders = [NSMutableSet new];
    servant = aServant;
  }
  return self;
}

- (void)build {
  NSMutableArray *scheduleEntries = [NSMutableArray new];
  for (ConfirmedOrderEntry *entry in entries) {
    Order *order = [self orderForConfirmedOrderEntry:entry];
    [order addEntry:entry];
    if ([self orderEntryCanBeDisplayedInSchedule:entry]) {
      ScheduleItem *item = [self scheduleItemFromOrderEntry:entry];
      [scheduleEntries addObject:item];
      item.order = order;
    }
  }
  NSSortDescriptor *byStartDateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES];
  [scheduleEntries sortUsingDescriptors:@[byStartDateSortDescriptor]];
  [self.delegate scheduleBuilder:self didBuildSchedule:scheduleEntries];
}

- (BOOL)orderEntryCanBeDisplayedInSchedule:(ConfirmedOrderEntry *)entry {
  BOOL isService = (entry.type == CatalogEntityTypeService);
  BOOL hasDateLimits = entry.startDate && entry.endDate;
  BOOL hasValidStatus = (entry.status >= 0) && (entry.status < OrderEntryStatusCount);
  BOOL isForCurrentServant = (entry.servant.ID == servant.ID);
  return isService && hasDateLimits && hasValidStatus && isForCurrentServant && entry.isCurrent;
}

- (ScheduleItem *)scheduleItemFromOrderEntry:(ConfirmedOrderEntry *)entry {
  ScheduleItem *item = [ScheduleItem new];
  item.startDate = entry.startDate;
  item.endDate = entry.endDate;
  item.customerName = entry.customer.fullName;
  item.status = entry.status;
  item.orderEntryID = entry.ID;
  return item;
}

- (Order *)orderForConfirmedOrderEntry:(ConfirmedOrderEntry *)entry {
  NSSet *orderWithID = [orders objectsPassingTest:^BOOL(Order *order, BOOL *stop) {
    return (order.ID == entry.orderID);
  }];
  Order *order = orderWithID.anyObject;
  if (!order) {
    order = [Order new];
    order.ID = entry.orderID;
    order.customer = entry.customer;
    [orders addObject:order];
  }
  return order;
}

@end
