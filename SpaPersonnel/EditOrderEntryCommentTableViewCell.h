//
//  EditOrderEntryCommentTableViewCell.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/23/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "EditOrderEntryCommentView.h"

@interface EditOrderEntryCommentTableViewCell : UITableViewCell

@property (weak, nonatomic) EditOrderEntryCommentView *theContentView;

- (void)setComment:(NSString *)comment;

- (void)setTextViewDelegate:(id <UITextViewDelegate>)delegate;

@end
