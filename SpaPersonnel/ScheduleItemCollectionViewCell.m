//
//  ScheduleItemCollectionViewCell.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/28/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ScheduleItemCollectionViewCell.h"
#import "UIColor+SpaPersonnel.h"

@implementation ScheduleItemCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
  if (self = [super initWithFrame:frame]) {
    [self setup];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    [self setup];
  }
  return self;
}

- (void)setup {
  self.layer.cornerRadius = 5.0f;
  self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.frame];
}

- (void)setBackgroundColorWithStatus:(OrderEntryStatus)status {
  if ((status == InProgress) || (status == NotStarted)) {
    self.backgroundColor = [UIColor activeScheduleItemColor];
  } else if ((status == Finished) || (status == Cancelled)){
    self.backgroundColor = [UIColor inactiveScheduleItemColor];
  } else if (status == AwaitingForConfirmation) {
    self.backgroundColor = [UIColor awaitingForConfirmationScheduleItemColor];
  }
}

@end
