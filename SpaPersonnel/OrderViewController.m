//
//  OrderViewController.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/27/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "OrderViewController.h"
#import "ConfirmedOrderEntry.h"
#import "RecommendedOrderEntryTableViewCell.h"
#import "ConfirmedOrderEntryTableViewCell.h"
#import "TableViewSectionHeaderWithButton.h"
#import "ServerManager.h"
#import "CatalogItemsListViewController.h"
#import "OrderManager.h"
#import "ConfirmedOrderEntriesViewController.h"
#import "RecommendedOrderEntriesViewController.h"
#import "ClientInfoViewController.h"
#import "NSString+SpaPersonnel.h"
#import "SpaModalPresentationController.h"

static NSString * const ConfirmedEntriesSegueIdentifier = @"confirmedEntries";
static NSString * const RecommendedEntriesSegueIdentifier = @"recommendedEntries";

static NSString * const ClientInfoViewControllerStoryboardID = @"ClientInfoViewController";

@interface OrderViewController () <OrderTableViewControllerDelegate, ClientInfoViewControllerDelegate, UIViewControllerTransitioningDelegate, UIViewControllerTransitioningDelegate>

@end

@implementation OrderViewController {
  NSDateFormatter *dateFormatter;
  NSNumberFormatter *quantityFormatter;
  
  UITextField *firstResponderTextField;
  
  ConfirmedOrderEntriesViewController *confirmedEntriesController;
  RecommendedOrderEntriesViewController *recommendedEntriesController;
  
  ScheduleItem *currentScheduleItem;
  
  SpaModalPresentationController *modalPresentationController;
}

- (void)viewDidLoad {
  [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)fillWithScheduleItem:(ScheduleItem *)scheduleItem {
  currentScheduleItem = scheduleItem;
  Order *order = currentScheduleItem.order;
  
  clientNameLabel.text = [NSString clietFullNameStringForCustomer:order.customer];
  clientSexAndAgeLabel.text = [NSString sexAndAgeStringForCustomer:order.customer];
  clientInfoButton.hidden = !order;
  
  [confirmedEntriesController fillWithOrder:order];
  [recommendedEntriesController fillWithOrder:order];
}

#pragma mark - Actions

- (IBAction)userDidTapUpdateData:(id)sender {
  [self.delegate orderViewController:self requestsUpdateWithScheduleItem:currentScheduleItem];
}

- (IBAction)userDidTapClientInfoButton:(id)sender {
  [self displayClientInfo];
}

#pragma mark - Handling Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:ConfirmedEntriesSegueIdentifier]) {
    [self setupConfirmedEntriesController:segue.destinationViewController];
  }
  if ([segue.identifier isEqualToString:RecommendedEntriesSegueIdentifier]) {
    [self setupRecommendedEntriesController:segue.destinationViewController];
  }
}

- (void)setupConfirmedEntriesController:(ConfirmedOrderEntriesViewController *)rootController {
  rootController.services = self.services;
  confirmedEntriesController = rootController;
  confirmedEntriesController.delegate = self;
}

- (void)setupRecommendedEntriesController:(RecommendedOrderEntriesViewController *)rootController {
  rootController.services = self.services;
  recommendedEntriesController = rootController;
  recommendedEntriesController.delegate = self;
}

- (void)displayClientInfo {
  UIViewController *rootController = [self.storyboard instantiateViewControllerWithIdentifier:ClientInfoViewControllerStoryboardID];
  ClientInfoViewController *controller = (ClientInfoViewController *)rootController;
  if ([rootController isKindOfClass:[UINavigationController class]]) {
    UINavigationController *navController = (UINavigationController *)rootController;
    controller = ((ClientInfoViewController *)navController.topViewController);
  }
  controller.services = self.services;
  controller.delegate = self;
  [controller fillWithCustomer:currentScheduleItem.order.customer orderID:currentScheduleItem.order.ID];
  
  //custom modal presentation
  rootController.modalPresentationStyle = UIModalPresentationCustom;
  rootController.transitioningDelegate = self;
  [self presentViewController:rootController animated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
  modalPresentationController = [[SpaModalPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
  modalPresentationController.offsetDataSource = (UIViewController<PresentedViewOffsetDataSource> *)presented;
  return modalPresentationController;
}

#pragma mark - OrderViewControllerDelegate

- (void)orderViewControllerDidChangeOrder:(UIViewController *)controller {
  [self.delegate orderViewController:self didChangeScheduleItem:currentScheduleItem];
}

- (void)orderViewControllerDidMadeOrderChangeRequiringFullUpdate:(UIViewController *)controller {
  [self.delegate orderViewController:self requestsUpdateWithScheduleItem:currentScheduleItem];
}

#pragma mark - ClientInfoViewControllerDelegate

- (void)clientInfoViewControllerUserWantsToDismiss:(ClientInfoViewController *)controller {
  [self dismissViewControllerAnimated:YES completion:nil];
}

@end
