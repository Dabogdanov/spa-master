//
//  ConfirmedOrderEntriesViewController.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/15/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServicesFactory.h"
#import "Order.h"
#import "OrderViewController.h"

@interface ConfirmedOrderEntriesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
  IBOutlet UIView *headerView;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) ServicesFactory *services;

@property (weak, nonatomic) id <OrderTableViewControllerDelegate> delegate;

- (void)fillWithOrder:(Order *)order;

@end
