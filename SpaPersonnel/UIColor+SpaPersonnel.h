//
//  UIColor+SpaPersonnel.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/28/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface UIColor (SpaPersonnel)

+ (UIColor *) appMainColor;

+ (UIColor *)activeScheduleItemColor;
+ (UIColor *)inactiveScheduleItemColor;
+ (UIColor *)awaitingForConfirmationScheduleItemColor;
+ (UIColor *)selectedScheduleItemColor;
+ (UIColor *)selectedScheduleItemBorderColor;

+ (UIColor *)defaultButtonTextColor;
+ (UIColor *)highlightedButtonTextColor;
+ (UIColor *)buttonDefaultRedColor;
+ (UIColor *)buttonHighlightedRedColor;

+ (UIColor *)mainScreenBackgroundColor;
+ (UIColor *)navigationItemBackgroundColor;
+ (UIColor *)navigationItemTintColor;

+ (UIColor *)orderScreenHeaderColor;
+ (UIColor *)orderScreenSectionHeaderColor;
+ (UIColor *)orderScreenItemBackgroudColor;
+ (UIColor *)orderScreenItemIsBeginProcessedBackgroudColor;
+ (UIColor *)dividerLineColor;

+ (UIColor *)colorWithHex:(int)hex;
+ (UIColor *)colorWithRed255:(CGFloat)red green255:(CGFloat)green blue255:(CGFloat)blue alpha:(CGFloat)alpha;

@end
