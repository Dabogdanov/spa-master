//
//  MainViewController.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/27/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "MainViewController.h"
#import "OrderViewController.h"
#import "ScheduleProviderAdapter.h"
#import "ScheduleBuilderAdapter.h"
#import "SpaPersonnelApplication.h"
#import "UIColor+SpaPersonnel.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

static NSString * const kShowScheduleSegueName = @"schedule";
static NSString * const kShowOrderSegueName = @"order";

@interface MainViewController () <ScheduleViewControllerDelegate, OrderViewControllerDelegate>

@end

@implementation MainViewController {
  OrderViewController *orderController;
  ScheduleViewController *scheduleController;
  ScheduleProviderAdapter *scheduleProvider;
  ScheduleBuilderAdapter *scheduleBuilder;
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self
   
                                                  name:kApplicationDidTimeoutNotification
                                                object:nil];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidTimeOut:)
                                                 name:kApplicationDidTimeoutNotification
                                               object:nil];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self setupNavigationBar];
  // Do any additional setup after loading the view.
    
    NSString *mainIconURLString = [[NSUserDefaults standardUserDefaults] objectForKey: spMainIconURLSettingsKey];
    if(mainIconURLString){
        NSString *imageName = [mainIconURLString lastPathComponent];
        NSString *appDocumentFolder = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex: 0];
        NSString *cacheImagePath = [appDocumentFolder stringByAppendingPathComponent: imageName];
        if([[NSFileManager defaultManager] fileExistsAtPath: cacheImagePath]){
            self.mainIconView.image = [UIImage imageWithContentsOfFile: cacheImagePath];
        }
        else{
            NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString: mainIconURLString]
                                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                      timeoutInterval:60];
            
            MainViewController *selfController __weak = self;
            [self.mainIconView setImageWithURLRequest: imageRequest
                                     placeholderImage: nil
                                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                  if(image){
                                                      [UIImagePNGRepresentation(image) writeToFile: cacheImagePath atomically:YES];
                                                      selfController.mainIconView.image = image;
                                                  }
                                              }
                                              failure: nil];

        }
    }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:kShowScheduleSegueName]) {
    scheduleController = segue.destinationViewController;
    scheduleController.services = self.services;
    scheduleController.delegate = self;
  }
  if ([segue.identifier isEqualToString:kShowOrderSegueName]) {
    orderController = segue.destinationViewController;
    orderController.services = self.services;
    orderController.delegate = self;
  }
}

- (void)logout {
  [self.services logout];
  [self.delegate mainViewControllerUserWantsToDismiss:self];
}

#pragma mark - Notifications

- (void)applicationDidTimeOut:(NSNotification *)notification {
  [self logout];
}

#pragma mark - ScheduleViewControllerDelegate

- (void)scheduleViewController:(ScheduleViewController *)scheduleViewController didSelectScheduleItem:(ScheduleItem *)item {
  [orderController fillWithScheduleItem:item];
}

- (void)scheduleViewController:(ScheduleViewController *)scheduleViewController didSelectDate:(NSDate *)date {
  [self fetchScheduleForDate:date];
}

#pragma mark - OrderViewControllerDelegate

- (void)orderViewController:(OrderViewController *)controller didChangeScheduleItem:(ScheduleItem *)anItem {
  [self reloadWithModifiedScheduleItem:anItem];
}

- (void)orderViewController:(OrderViewController *)controller requestsUpdateWithScheduleItem:(ScheduleItem *)anItem {
  [self resyncScheduleAndSelectScheduleItem:anItem];
}

#pragma mark - Private Methods

- (void)setupNavigationBar {
  
  UIView *theTitleView = self.navigationItem.titleView;
  
  shopNameLabel.textColor = [UIColor appMainColor];
  shopNameLabel.font = [UIFont fontWithName: APP_FONT_NAME size: 25.0f];
    
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  NSString *titleString = [defaults objectForKey: @"mainTitle"];
  if(titleString == nil){
    titleString = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleName"];
  }
  shopNameLabel.text = titleString;
  servantNameLabel.text = self.services.servant.fullName;
  
  // code below adjusts navigation item's titleView since it doesn't adjust automatically
  CGSize shopNameLabelOptimumSize = [servantNameLabel systemLayoutSizeFittingSize:UILayoutFittingExpandedSize];
  CGSize servantNameLabelOptimumSize = [servantNameLabel systemLayoutSizeFittingSize:UILayoutFittingExpandedSize];
  theTitleView.frame = CGRectMake(CGRectGetMinX(theTitleView.frame),
                                  CGRectGetMinY(theTitleView.frame),
                                  MAX(shopNameLabelOptimumSize.width, servantNameLabelOptimumSize.width),
                                  CGRectGetHeight(theTitleView.frame));
}

- (void)fetchScheduleForDate:(NSDate *)date {
  [scheduleController updateWithScheduleItems:nil selectedItem:nil shouldIndicateLoading:NO];
  [orderController fillWithScheduleItem:nil];
  MainViewController * __weak safeSelf = self;
  scheduleProvider = [ScheduleProviderAdapter asyncGetScheduleWithDate:date completionBlock:^(NSArray *schedule, NSDate *date, NSError *error) {
    if (!error) {
      [safeSelf didReceiveSchedule:schedule forDate:date];
    } else {
      [safeSelf didFailToGetSchedule:error forDate:date];
    }
  } servicesFactory:self.services];
}

- (void)updateScheduleForDate:(NSDate *)date withItemToSelectAfter:(ScheduleItem *)itemToSelect silent:(BOOL)isSilent {
  if (!isSilent) {
    [scheduleController updateWithScheduleItems:nil selectedItem:nil shouldIndicateLoading:YES];
    [orderController fillWithScheduleItem:nil];
  }
  MainViewController * __weak safeSelf = self;
  scheduleProvider = [ScheduleProviderAdapter asyncGetScheduleWithDate:date completionBlock:^(NSArray *schedule, NSDate *date, NSError *error) {
    if (!error) {
      [safeSelf didUpdateSchedule:schedule forDate:date withItemToSelectAfter:itemToSelect];
    } else {
      [safeSelf didFailToGetSchedule:error forDate:date];
    }
  } servicesFactory:self.services];
}

- (ScheduleItem *)pickScheduleItemFromSchedule:(NSArray *)scheduleEntries {
  ScheduleItem *pickedItem = nil;
  
  for (NSUInteger ui = 0; ui < scheduleEntries.count; ui++) {
    ScheduleItem *entry = scheduleEntries[ui];
    if (entry.status != Finished && entry.status != Cancelled) {
      pickedItem = entry;
      break;
    }
  }
  return pickedItem;
}

- (void)reloadWithModifiedScheduleItem:(ScheduleItem *)anItem {
  scheduleBuilder = [ScheduleBuilderAdapter asyncBuildScheduleItemsWithOrder:anItem.order
                                                             completionBlock:^(NSArray *scheduleItems) {
    NSArray *entries = [self updatedSchedule:scheduleController.entries withNewScheduleItems:scheduleItems];
    NSUInteger newIndex = [entries indexOfObjectPassingTest:^BOOL(ScheduleItem *item, NSUInteger idx, BOOL *stop) {
      return (anItem.orderEntryID == item.orderEntryID);
    }];
    if (newIndex != NSNotFound) {
      [scheduleController updateWithScheduleItems:entries selectedItem:entries[newIndex] shouldIndicateLoading:NO];
    } else {
      NSUInteger newOrderIndex = [entries indexOfObjectPassingTest:^BOOL(ScheduleItem *item, NSUInteger idx, BOOL *stop) {
        return (anItem.order.ID == item.order.ID);
      }];
      if (newOrderIndex != NSNotFound) {
        [scheduleController updateWithScheduleItems:entries selectedItem:entries[newOrderIndex] shouldIndicateLoading:NO];
      } else {
        ScheduleItem *item = [self pickScheduleItemFromSchedule:entries];
        [scheduleController updateWithScheduleItems:entries selectedItem:item shouldIndicateLoading:NO];
        [orderController fillWithScheduleItem:item];
      }
    }
  }
                                                                     servant:self.services.servant];
}

- (void)resyncScheduleAndSelectScheduleItem:(ScheduleItem *)oldScheduleItem {
  [self updateScheduleForDate:scheduleController.selectedDate withItemToSelectAfter:oldScheduleItem silent:NO];
}

- (NSArray *)updatedSchedule:(NSArray *)oldSchedule withNewScheduleItems:(NSArray *)newItems {
  NSMutableArray *newEntries = [oldSchedule mutableCopy];
  
  // removing duplicates
  // probably will need optimisation. Since both arrays are sorted, could be done in linear time.
  NSMutableIndexSet *toRemoveIndexSet = [[NSMutableIndexSet alloc] init];
  for (ScheduleItem *newItem in newItems) {
    for (NSUInteger ui = 0; ui < oldSchedule.count; ui++) {
      ScheduleItem *item = oldSchedule[ui];
      if (item.order.ID == newItem.order.ID) {
        [toRemoveIndexSet addIndex:ui];
      }
    }
  }
  
  [newEntries removeObjectsAtIndexes:toRemoveIndexSet];
  
  [newEntries addObjectsFromArray:newItems];
  NSSortDescriptor *byStartDateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES];
  [newEntries sortUsingDescriptors:@[byStartDateSortDescriptor]];
  
  return newEntries;
}

- (void)didReceiveSchedule:(NSArray *)schedule forDate:(NSDate *)date {
  if ([scheduleController.selectedDate isEqual:date]) {
    ScheduleItem *itemToSelect = [self pickScheduleItemFromSchedule:schedule];
    [scheduleController updateWithScheduleItems:schedule selectedItem:itemToSelect shouldIndicateLoading:NO];
    [orderController fillWithScheduleItem:itemToSelect];
  }
}

- (void)didUpdateSchedule:(NSArray *)schedule forDate:(NSDate *)date withItemToSelectAfter:(ScheduleItem *)itemToSelect {
  if ([scheduleController.selectedDate isEqual:date]) {
    [scheduleController updateWithScheduleItems:schedule selectedItem:itemToSelect shouldIndicateLoading:NO];
    [orderController fillWithScheduleItem:[scheduleController selectedItem]];
  }
}

- (void)didFailToGetSchedule:(NSError *)error forDate:(NSDate *)date {
  if ([scheduleController.selectedDate isEqual:date]) {
    [scheduleController updateWithScheduleItems:nil selectedItem:nil shouldIndicateLoading:NO];
    [orderController fillWithScheduleItem:nil];
    NSLog(@"error fetching schedule: %@", error.localizedDescription);
  }
}

#pragma mark Actions

- (IBAction)userDidPressExit:(id)sender {
  [self logout];
}

@end
