//
//  SpaButton.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/2/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface SpaButton : UIButton

// Instance uses this value to set rounded corners on initialization
// subclasses may override this method to provide different corner radius
// for a view loaded from nib file.
- (CGFloat)cornerRadius;

@end
