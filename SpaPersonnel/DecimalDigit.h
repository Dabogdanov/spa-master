//
//  Digit.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/18/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface DecimalDigit : NSObject
+ (instancetype)decimalDigitWithUnsignedInteger:(NSUInteger)aUInt;
- (NSUInteger)value;
- (NSString *)description;

@end
