//
//  QuantityTableViewCell.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/23/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@class QuantityTableViewCell;

@protocol QuantityTableViewCellDelegate <NSObject>
- (void)quantityTableViewCellUserDidPressAddButton:(QuantityTableViewCell *)cell;
- (void)quantityTableViewCellUserDidPressSubtractButton:(QuantityTableViewCell *)cell;

@end

@interface QuantityTableViewCell : UITableViewCell {
  IBOutlet UIButton *addButton;
  IBOutlet UIButton *subtractButton;
  IBOutlet UILabel *diasbledReasonLabel;
}
@property (weak, nonatomic) IBOutlet UITextField *quantityTextField;

@property (weak, nonatomic) id <QuantityTableViewCellDelegate> delegate;
@property id <UITextFieldDelegate> quantityTextFieldDelegate;

- (void)setCellEnabled:(BOOL)enabled;
- (void)setQuantityIsValid:(BOOL)valid;
//- (void)setDisabledReasonString:(NSString *)string;

@end
