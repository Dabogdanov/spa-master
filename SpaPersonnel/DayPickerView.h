//
//  DayPickerView.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/27/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@class DayPickerView;

@protocol DayPickerViewDelegate <NSObject>
- (void)dayPickerViewUserDidPressPreviousDateButton:(DayPickerView *)view;
- (void)dayPickerViewUserDidPressNextDateButton:(DayPickerView *)view;

@end

@interface DayPickerView : UIView {
  IBOutlet UIButton * __weak previousDateButton;
  IBOutlet UIButton * __weak nextDateButton;
}
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet id <DayPickerViewDelegate> delegate;

@end
