//
//  ServerManager.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServerManager.h"
#import "Constants.h"
#import "Session.h"
#import "CatalogEntity.h"
#import "Order.h"
#import "Customer.h"

#import "CustomerCommentary.h"

NSString* const kSettingsApiUrl = @"apiUrl";
NSString * const kUserDidAuthorize = @"SpaPersonnelUserDidAuthorize";
NSString * const kUserDidLogout = @"SpaPersonnelUserDidLogOut";

@interface ServerManager()
@property (nonatomic, readonly) NSString *apiBaseUrlString;
@end

@implementation ServerManager {
  NSDateFormatter *dateFormatter;
  RKObjectManager *objectManager;
}

- (void)setNewSession:(Session *)newSession {
  _session = newSession;
  [[self manager].HTTPClient setDefaultHeader:kAuthHeaderField value:newSession.token];
  [[NSNotificationCenter defaultCenter] postNotificationName:kUserDidAuthorize
                                                      object:nil
                                                    userInfo:nil];
}

- (void)logout {
  _session = nil;
  [[NSNotificationCenter defaultCenter] postNotificationName:kUserDidLogout
                                                      object:nil
                                                    userInfo:nil];
}

- (void)setupOrderEntriesMappingForManager:(RKObjectManager *)manager {
  RKObjectMapping *customerMapping = [[RKObjectMapping alloc] initWithClass:[Customer class]];
  [customerMapping addAttributeMappingsFromDictionary:@{@"ID": @"ID", @"FullName": @"fullName", @"Sex": @"sex", @"Age": @"age"}];
  
  RKObjectMapping *servantMapping = [Servant toServantMapping];
  
  RKObjectMapping *orderEntryMapping = [[RKObjectMapping alloc] initWithClass:[ConfirmedOrderEntry class]];
  [orderEntryMapping addAttributeMappingsFromDictionary:@{@"ID": @"ID",
                                                       @"OrderID": @"orderID",
                                                       @"ItemID": @"itemID",
                                                       @"Quantity": @"quantity",
                                                       @"Place": @"place",
                                                       @"TimeStart": @"startDate",
                                                       @"TimeEnd": @"endDate",
                                                       @"Duration": @"duration",
                                                       @"Comment": @"comment",
                                                       @"Price": @"price",
                                                       @"Status": @"status",
                                                       @"OpenedTime": @"openedDate",
                                                       @"ClosedTime": @"closedDate",
                                                       @"IsCurrent": @"isCurrent",
                                                       @"Type": @"type"}];
  
  
  [orderEntryMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"Customer"
                                                                                 toKeyPath:@"customer"
                                                                               withMapping:customerMapping]];
  
  [orderEntryMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"Servant"
                                                                                    toKeyPath:@"servant"
                                                                                  withMapping:servantMapping]];
  
  // TODO: needs path pattern for variable token and data
  RKResponseDescriptor *getOrderEntriesResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:orderEntryMapping
                                                                                                         method:RKRequestMethodGET
                                                                                                    pathPattern:nil
                                                                                                        keyPath:@"OrderEntries"
                                                                                                    statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
  
  
  RKResponseDescriptor *postAndPutAndDeleteOrderEntriesResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:orderEntryMapping
                                                                                                                method:RKRequestMethodPOST | RKRequestMethodPUT | RKRequestMethodDELETE
                                                                                                           pathPattern:nil
                                                                                                               keyPath:@"Entry"
                                                                                                           statusCodes:RKStatusCodeIndexSetForClass (RKStatusCodeClassSuccessful)];

  [manager addResponseDescriptorsFromArray:@[getOrderEntriesResponseDescriptor, postAndPutAndDeleteOrderEntriesResponseDescriptor]];
  
  RKObjectMapping *requestOrderEntryMapping = [RKObjectMapping mappingForClass:[NSMutableDictionary class]];
  [requestOrderEntryMapping addAttributeMappingsFromDictionary:@{@"orderID": @"OrderID",
                                                                 @"itemID": @"ItemID",
                                                                 @"quantity": @"Quantity",
                                                                 @"comment": @"Commentary"}];
  
  RKRequestDescriptor *postOrderEntriesRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:requestOrderEntryMapping
                                                                                                 objectClass:[ConfirmedOrderEntry class]
                                                                                                 rootKeyPath:nil
                                                                                                      method:RKRequestMethodPOST];
  [manager addRequestDescriptor:postOrderEntriesRequestDescriptor];
}

- (void)setupSessionMappingForManager:(RKObjectManager *)manager {
  RKObjectMapping *servantMapping = [Servant toServantMapping];
  
  RKObjectMapping *sessionMapping = [[RKObjectMapping alloc] initWithClass:[Session class]];
  [sessionMapping addAttributeMappingsFromDictionary:@{@"Session": @"token"}];
  
  [sessionMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"Servant"
                                                                                 toKeyPath:@"servant"
                                                                               withMapping:servantMapping]];
  
  NSString *pathPattern = [NSString stringWithFormat:@"%@/shops/%@/sessions", kAPIVersion, kShopID];
  RKResponseDescriptor *sessionResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:sessionMapping
                                                                                                 method:RKRequestMethodPOST
                                                                                            pathPattern:pathPattern
                                                                                                keyPath:nil
                                                                                            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
  [manager addResponseDescriptor:sessionResponseDescriptor];
}

- (void)setupCatalogEntitiesListMappingForManager:(RKObjectManager *)manager {
  RKObjectMapping *itemMapping = [[RKObjectMapping alloc] initWithClass:[CatalogEntity class]];
  [itemMapping addAttributeMappingsFromDictionary:@{@"Name": @"name", @"Description": @"fullDescription", @"Visible": @"isVisible", @"ID": @"ID", @"Duration": @"duration", @"Price": @"price", @"Type": @"type"}];
  
  RKObjectMapping *sessionMapping = [[RKObjectMapping alloc] initWithClass:[CatalogEntitiesList class]];
  [sessionMapping addAttributeMappingsFromDictionary:@{@"ModifyDate": @"catalogVersion"}];
  
  [sessionMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"Items"
                                                                                 toKeyPath:@"catalogEntities"
                                                                               withMapping:itemMapping]];

  NSString *pathPattern = [NSString stringWithFormat:@"%@/shops/%@/items", kAPIVersion, kShopID];
  RKResponseDescriptor *sessionResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:sessionMapping
                                                                                                 method:RKRequestMethodGET
                                                                                            pathPattern:pathPattern
                                                                                                keyPath:nil
                                                                                            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
  [manager addResponseDescriptor:sessionResponseDescriptor];
}

- (void)setupCustomerCommentaryMappingForManager:(RKObjectManager *)manager {
  RKObjectMapping *customerCommentaryMapping = [[RKObjectMapping alloc] initWithClass:[CustomerCommentary class]];
  [customerCommentaryMapping addAttributeMappingsFromDictionary:@{@"CommentsHtml": @"customerInfoHTML"}];
  NSString *pathPattern = [NSString stringWithFormat:@"%@/customers/:customerID/comments", kAPIVersion];
  RKResponseDescriptor *customerCommentaryResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:customerCommentaryMapping
                                                                                                            method:RKRequestMethodGET | RKRequestMethodPOST
                                                                                                       pathPattern:pathPattern
                                                                                                           keyPath:nil
                                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
  [manager addResponseDescriptor:customerCommentaryResponseDescriptor];
}

- (RKObjectManager *)manager
{
  if (!objectManager) {
    objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString: self.apiBaseUrlString]];
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    [self setupSessionMappingForManager:objectManager];
    [self setupCatalogEntitiesListMappingForManager:objectManager];
    [self setupOrderEntriesMappingForManager:objectManager];
    [self setupCustomerCommentaryMappingForManager:objectManager];
  }
  return objectManager;
}

- (NSError *)appErrorWithResponseData:(NSData *)responseData error:(NSError *)error {
  NSError *newError = error;
  if (responseData) {
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    NSUInteger code = 0;
    NSString *errorMessage = nil;
    if ([json isKindOfClass:[NSDictionary class]]) {
      errorMessage = [json objectForKey:@"Error"];
      code = ((NSNumber *)[json objectForKey:@"ErrorID"]).unsignedIntegerValue;
    }
    if (errorMessage) {
      NSMutableDictionary *errorDetails = [NSMutableDictionary dictionary];
      [errorDetails setObject:errorMessage forKey:NSLocalizedDescriptionKey];
      newError = [NSError errorWithDomain:error.domain code:code userInfo:errorDetails];
    }
  }
  return newError;
}

- (void)authWithPassword:(NSString *)password
                deviceID:(NSString *)deviceID
                 success:(void (^)(Session *))success
                 failure:(void (^)(NSError *))failure {
  NSDictionary *parameters = @{@"Key" : password,
                               @"DeviceID" : deviceID};
  NSString *path = [NSString stringWithFormat:@"%@/shops/%@/sessions", kAPIVersion, kShopID];
  ServerManager * __weak weakSelf = self;
  [[self manager] postObject:nil path:path parameters:parameters success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
    id result = mappingResult.firstObject;
    if ([result isKindOfClass:[Session class]]) {
      Session *newSession = result;
      [weakSelf setNewSession:newSession];
      success(newSession);
    }
  } failure:^(RKObjectRequestOperation *operation, NSError *error) {
    NSData *responseData = operation.HTTPRequestOperation.responseData;
    NSError *newError = [self appErrorWithResponseData:responseData error:error];
    failure(newError);
  }];
}

- (void)fetchOrderEntriesForDate:(NSDate *)date success:(void (^)(NSArray *))success failure:(void (^)(NSError *))failure {
  NSString *path = [NSString stringWithFormat:@"%@/entries?date=%@", kAPIVersion, [[self dateFormatter] stringFromDate:date]];
  [[self manager] getObject:nil path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
    success(mappingResult.array);
  } failure:^(RKObjectRequestOperation *operation, NSError *error) {
    NSData *responseData = operation.HTTPRequestOperation.responseData;
    NSError *newError = [self appErrorWithResponseData:responseData error:error];
    failure(newError);
  }];
}

- (void)checkCatalogUpdatesWithVersion:(NSString *)version
                               success:(void (^)(CatalogEntitiesList *))success
                               failure:(void (^)(NSError *))failure {
  NSString *basicPath = [NSString stringWithFormat:@"%@/shops/%@/items", kAPIVersion, kShopID];
  NSMutableString *fullPath = [NSMutableString stringWithString:basicPath];
  if (version) {
    [fullPath appendFormat:@"?modifyDate=%@", version];
  }
  [[self manager] getObject:nil path:fullPath parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
    success(mappingResult.firstObject);
  } failure:^(RKObjectRequestOperation *operation, NSError *error) {
    NSData *responseData = operation.HTTPRequestOperation.responseData;
    NSError *newError = [self appErrorWithResponseData:responseData error:error];
    failure(newError);
  }];
}

- (void)addOrderEntryToOrderWithID:(int64_t)orderID
                        withItemID:(int64_t)itemID
                           comment:(NSString *)commentary
                          quantity:(NSDecimalNumber *)quantity
                           success:(void (^)(ConfirmedOrderEntry *))success
                           failure:(void (^)(NSError *))failure {
  NSMutableDictionary *parameters = [NSMutableDictionary new];
  parameters[@"OrderID"] = @(orderID);
  parameters[@"ItemID"] = @(itemID);
  if (commentary) {
    parameters[@"Commentary"] = commentary;
  }
  if (quantity) {
    parameters[@"Quantity"] = quantity;
  }
  NSString *path = [NSString stringWithFormat:@"%@/entries", kAPIVersion];
  [[self manager] postObject:nil path:path parameters:parameters success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
    success(mappingResult.firstObject);
  } failure:^(RKObjectRequestOperation *operation, NSError *error) {
    NSData *responseData = operation.HTTPRequestOperation.responseData;
    NSError *newError = [self appErrorWithResponseData:responseData error:error];
    failure(newError);
  }];
}

- (void)editOrderEntryWithID:(int64_t)ID
                   newStatus:(OrderEntryStatus)status
                   newItemID:(int64_t)itemID
                  newComment:(NSString *)commentary
                 newQuantity:(NSDecimalNumber *)quantity
                     success:(void (^)(ConfirmedOrderEntry *entry))success
                     failure:(void (^)(NSError *error))failure {
  NSMutableDictionary *parameters = [NSMutableDictionary new];
  parameters[@"ItemID"] = @(itemID);
  parameters[@"Status"] = @(status);
  if (commentary) {
    parameters[@"Commentary"] = commentary;
  }
  if (quantity) {
    parameters[@"Quantity"] = quantity;
  }
  NSString *path = [NSString stringWithFormat:@"%@/entries/%lld", kAPIVersion, ID];
  [[self manager] putObject:nil path:path parameters:parameters success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
    success(mappingResult.firstObject);
  } failure:^(RKObjectRequestOperation *operation, NSError *error) {
    NSData *responseData = operation.HTTPRequestOperation.responseData;
    NSError *newError = [self appErrorWithResponseData:responseData error:error];
    failure(newError);
  }];
}

- (void)deleteOrderEntryWithID:(int64_t)ID
                       success:(void (^)(ConfirmedOrderEntry *))success
                       failure:(void (^)(NSError *))failure {
  NSString *path = [NSString stringWithFormat:@"%@/entries/%lld", kAPIVersion, ID];
  [[self manager] deleteObject:nil path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
    success(mappingResult.firstObject);
  } failure:^(RKObjectRequestOperation *operation, NSError *error) {
    NSData *responseData = operation.HTTPRequestOperation.responseData;
    NSError *newError = [self appErrorWithResponseData:responseData error:error];
    failure(newError);
  }];
}

- (void)asyncAppendString:(NSString *)string
  toInfoForCustomerWithID:(int64_t)customerID
                  orderID:(int64_t)orderID
                  success:(void (^)(NSString *))success
                  failure:(void (^)(NSError *))failure {
  NSString *basicPath = [NSString stringWithFormat:@"%@/customers/%lld/comments", kAPIVersion, customerID];
  NSMutableDictionary *parameters = [NSMutableDictionary new];
  parameters[@"Commentary"] = string;
  parameters[@"OrderID"] = @(orderID);
  [[self manager] postObject:nil path:basicPath parameters:parameters success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
    CustomerCommentary *commentary = mappingResult.firstObject;
    success(commentary.customerInfoHTML);
  } failure:^(RKObjectRequestOperation *operation, NSError *error) {
    NSData *responseData = operation.HTTPRequestOperation.responseData;
    NSError *newError = [self appErrorWithResponseData:responseData error:error];
    failure(newError);
  }];
}

- (void)asyncGetInfoForCustomerWithID:(int64_t)ID
                              success:(void (^)(NSString *))success
                              failure:(void (^)(NSError *))failure {
  NSString *basicPath = [NSString stringWithFormat:@"%@/customers/%lld/comments", kAPIVersion, ID];
  [[self manager] getObject:nil path:basicPath parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
    CustomerCommentary *commentary = mappingResult.firstObject;
    success(commentary.customerInfoHTML);
  } failure:^(RKObjectRequestOperation *operation, NSError *error) {
    NSData *responseData = operation.HTTPRequestOperation.responseData;
    NSError *newError = [self appErrorWithResponseData:responseData error:error];
    failure(newError);
  }];
}

#pragma mark Date Formatter

- (NSDateFormatter *)dateFormatter {
  if (!dateFormatter) {
    dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"yyyy'-'MM'-'dd";
  }
  return dateFormatter;
}

#pragma mark properties

- (NSString*) apiBaseUrlString{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *apiURLPreference = [defaults objectForKey: kSettingsApiUrl];
    if(apiURLPreference && apiURLPreference.length > 0){
        return [apiURLPreference stringByAppendingString:@"/"];
    }
    else{
        return kBaseDefaultURL;
    }
}
@end
