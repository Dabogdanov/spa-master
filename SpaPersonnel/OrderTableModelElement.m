//
//  OrderTableModelElement.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/8/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "OrderTableModelElement.h"

@implementation OrderTableModelElement

- (instancetype)initWithConfirmedOrderEntry:(ConfirmedOrderEntry *)anEntry {
  if (self = [super init]) {
    self.entry = anEntry;
  }
  return self;
}

@end
