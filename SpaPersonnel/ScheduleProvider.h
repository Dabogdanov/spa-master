//
//  ScheduleProvider.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Session.h"
#import "ServerManager.h"

@class ScheduleProvider;

@protocol ScheduleProviderDelegate <NSObject>
- (void)scheduleProvider:(ScheduleProvider *)provider didCreateSchedule:(NSArray *)schedule forDate:(NSDate *)date;
- (void)scheduleProvider:(ScheduleProvider *)provider didFailWithError:(NSError *)error;

@end

@interface ScheduleProvider : NSObject
@property (weak, nonatomic) id <ScheduleProviderDelegate> delegate;
- (instancetype)initWithServerManager:(ServerManager *)manager;
- (void)createScheduleForDate:(NSDate *)date;

@end
