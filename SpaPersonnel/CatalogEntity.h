//
//  CatalogEntity.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/21/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

typedef NS_ENUM(int, CatalogEntityType) {
  CatalogEntityTypeService = 0,
  CatalogEntityTypeItem
};

@interface CatalogEntity : NSObject
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *fullDescription;
@property (assign, nonatomic) BOOL isVisible;
@property (assign, nonatomic) int64_t ID;
@property (assign, nonatomic) int64_t duration;
@property (copy, nonatomic) NSString *price;
@property (assign, nonatomic) CatalogEntityType type;

@end
