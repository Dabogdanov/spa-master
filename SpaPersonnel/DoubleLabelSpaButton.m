//
//  DoubleLabelSpaButton.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/4/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "DoubleLabelSpaButton.h"
#import "UIColor+SpaPersonnel.h"

@implementation DoubleLabelSpaButton

- (instancetype)initWithFrame:(CGRect)frame {
  if (self = [super initWithFrame:frame]) {
    [self initialSetup];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    [self initialSetup];
  }
  return self;
}

- (void)setHighlighted:(BOOL)highlighted {
  [super setHighlighted:highlighted];
  [self setLabelsTextColorWithState:self.state];
}

- (void)setEnabled:(BOOL)enabled {
  [super setEnabled:enabled];
  [self setLabelsTextColorWithState:self.state];
}

- (void)initialSetup {
  [self setLabelsTextColorWithState:UIControlStateNormal];
}

- (void)setLabelsTextColorWithState:(UIControlState)state {
  self.topLabel.textColor = [self titleColorForState:state];
  self.topLabel.shadowColor = [self titleShadowColorForState:state];
  self.bottomLabel.textColor = [self titleColorForState:state];
  self.bottomLabel.shadowColor = [self titleShadowColorForState:state];
}

@end
