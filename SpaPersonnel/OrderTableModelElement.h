//
//  OrderTableModelElement.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/8/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ConfirmedOrderEntry.h"

@interface OrderTableModelElement : NSObject
@property (assign, nonatomic) BOOL isBeingModified;
@property (strong, nonatomic) ConfirmedOrderEntry *entry;

- (instancetype)initWithConfirmedOrderEntry:(ConfirmedOrderEntry *)entry;

@end
