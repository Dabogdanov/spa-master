//
//  AuthViewController.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "AuthViewController.h"
#import "Constants.h"
#import "DeviceIDService.h"
#import "MainViewController.h"
#import "PinBuilder.h"
#import "UIAlertView+SpaPersonnel.h"
#import "SpaPersonnelApplication.h"
#import "ViewWithActivityIndicator.h"
#import "UIView+SpaPersonnel.h"

static NSString * const kShowMainScreenSegueName = @"mainscreen";
static NSUInteger const PinLength = 4u;

@interface AuthViewController () <DeviceIDServiceDelegate, MainViewControllerDelegate>

@end

@implementation AuthViewController {
  DeviceIDService *deviceIDService;
  NSArray *sortedDigitViews;
  PinBuilder *pinBuilder;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    pinBuilder = [[PinBuilder alloc] initWithPinLength:PinLength];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  sortedDigitViews = [digitViews sortedArrayUsingComparator:^NSComparisonResult(DigitView *digitView1, DigitView *digitView2) {
    CGFloat minX1 = CGRectGetMinX(digitView1.frame);
    CGFloat minX2 = CGRectGetMinX(digitView2.frame);
    return (
             minX1 == minX2 ? NSOrderedSame :
            (minX1 < minX2 ? NSOrderedAscending : NSOrderedDescending)
            );
  }];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:kShowMainScreenSegueName]) {
    UINavigationController *navController = segue.destinationViewController;
    MainViewController *mainController = navController.viewControllers[0];
    mainController.services = sender;
    mainController.delegate = self;
  }
}

#pragma mark - Actions

// note that button tag must be equal to represented digit
// probably should be changed
- (IBAction)userDidPressDigitButton:(UIButton *)digitButton {
  NSAssert((digitButton.tag >=0) && (digitButton.tag <= 9), @"Pressed digit button must have a tag representing a valid decimal digit.");
  
  NSUInteger uDigit = digitButton.tag;
  DecimalDigit *digit = [DecimalDigit decimalDigitWithUnsignedInteger:uDigit];

  if ([pinBuilder appendDigit:digit]) {
    NSUInteger currentDigitCount = pinBuilder.currentDigitCount;
    if (currentDigitCount <= sortedDigitViews.count) {
      NSInteger currentDigitViewIndex = currentDigitCount - 1;
      DigitView *currentDigitView = sortedDigitViews[currentDigitViewIndex];
      [currentDigitView setActive:YES];
    }
    
    BOOL isLastDigit = (pinBuilder.currentDigitCount == PinLength);
    if (isLastDigit) {
      [self makeAuthAttemptWithKey:pinBuilder.pinString];
    }
  }
}

- (IBAction)userDidPressClearButton:(UIButton *)clearButon {
  [self removeLastDigit];
}

- (IBAction)getInfo:(id)sender {
  if ([DeviceIDService currentDeviceIDString]) {
    [self showDeviceIDAlert];
  } else {
    if (!deviceIDService) {
      deviceIDService = [[DeviceIDService alloc] init];
      deviceIDService.delegate = self;
    }
    [deviceIDService start];
  }
}

#pragma mark - DeviceIDServiceDelegate

- (void)deviceIDService:(DeviceIDService *)provider didReceiveNewDeviceID:(NSString *)aDeviceID {
  [self showDeviceIDAlert];
}

- (void)deviceIDService:(DeviceIDService *)provider didFailWithError:(NSError *)error {
  NSString *message = [NSString stringWithFormat:@"Произошла ошибка при получении уникального идентификатора устройства: %@", error.localizedDescription];
  [UIAlertView showAlertWithTitle:@"Ошибка"
                          message:message];
}

#pragma mark MainViewControllerDelegate

- (void)mainViewControllerUserWantsToDismiss:(MainViewController *)mainViewController {
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Authorization

- (void)makeAuthAttemptWithKey:(NSString *)key {
  [((ViewWithActivityIndicator *)self.view) lockInputAndShowActivityIndicator];
  ServicesFactory * __weak weakServices = self.services;
  [self.services authWithKey:key completion:^(NSError *error) {
    [self clearInputFields];
    [((ViewWithActivityIndicator *)self.view) unlockInputAndHideActivityIndicator];
    
    Servant *currentServant = weakServices.servant;
    if (currentServant) {
      [self performSegueWithIdentifier:kShowMainScreenSegueName sender:weakServices];
    }
    if (error) {
      dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка при авторизации"
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:@"ОК"
                                              otherButtonTitles:nil];
        [alert show];
      });
    }
  }];
}

- (void)clearInputFields {
  [pinBuilder clear];
  for (DigitView *digitView in sortedDigitViews) {
    [digitView clear];
    digitView.isActive = NO;
  }
}

- (void)removeLastDigit {
  NSUInteger currentDigitCount = pinBuilder.currentDigitCount;
  if ([pinBuilder removeLastDigit]) {
    if (currentDigitCount <= sortedDigitViews.count) {
      NSInteger currentDigitViewIndex = currentDigitCount - 1;
      DigitView *currentDigitView = sortedDigitViews[currentDigitViewIndex];
      [currentDigitView setActive:NO];
    }
  }
}

#pragma mark - Displaying Alerts

- (void)showDeviceIDAlert {
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Информация"
                                                  message:[NSString stringWithFormat:@"Идентификатор данного устройства: \"%@\"", [DeviceIDService currentDeviceIDString]]
                                                 delegate:nil
                                        cancelButtonTitle:@"ОК"
                                        otherButtonTitles:nil];
  [alert show];
}

@end
