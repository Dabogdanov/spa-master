//
//  DigitView.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/18/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "DigitView.h"

@implementation DigitView {
  DecimalDigit *currentDigit;
}

- (void)awakeFromNib {
  [self updateStateWithDigit:currentDigit isActive:self.isActive];
}

- (void)setActive:(BOOL)isActive {
  if (isActive != _isActive) {
    _isActive = isActive;
    [self updateStateWithDigit:currentDigit isActive:_isActive];
  }
}

- (void)setDigit:(DecimalDigit *)digit {
  currentDigit = digit;
  digitLabel.text = [NSString stringWithFormat:@"%@", digit];
  [self updateStateWithDigit:currentDigit isActive:self.isActive];
}

- (void)clear {
  currentDigit = nil;
  [self updateStateWithDigit:currentDigit isActive:self.isActive];
}

- (void)updateStateWithDigit:(DecimalDigit *)digit isActive:(BOOL)isActive {
  BOOL displaysNoDigit = !digit;
  digitLabel.hidden = displaysNoDigit;
  noDigitImageView.hidden = !displaysNoDigit;
  if (isActive) {
    noDigitImageView.image = [UIImage imageNamed:@"active_input_pin"];
  } else {
    noDigitImageView.image = [UIImage imageNamed:@"input_pin.png"];
  }
}

@end
