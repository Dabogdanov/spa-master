//
//  SpaPersonnelApplication.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/26/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "SpaPersonnelApplication.h"

NSString * const kApplicationDidTimeoutNotification = @"SpaPersonnelApplicationDidTimeOut";

@implementation SpaPersonnelApplication {
  NSTimer *idleTimer;
}

- (void)startTrackingIdleTime {
  // if idleTimer is valid, user touches tracking
  // is already in progress and this method should do nothing
  if (!idleTimer.valid) {
    [self resetIdleTimer];
  }
}

- (void)stopTrackingIdleTime {
  [self invalidateIdleTimer];
}

- (void)sendEvent:(UIEvent *)event {
  [super sendEvent:event];

  if (idleTimer.valid) {
    // Check to see if there was a touch event
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0) {
      UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
      if (phase == UITouchPhaseBegan) {
        [self resetIdleTimer];
      }
    }
  }
}

- (void)resetIdleTimer {
  [self invalidateIdleTimer];

  idleTimer = [NSTimer scheduledTimerWithTimeInterval:kIdleTimeInterval
                                               target:self
                                             selector:@selector(idleTimerExceeded)
                                             userInfo:nil
                                              repeats:NO];
  
}

- (void)idleTimerExceeded {
  [[NSNotificationCenter defaultCenter] postNotificationName:kApplicationDidTimeoutNotification
                                                      object:nil];
}

- (void)invalidateIdleTimer {
  if (idleTimer) {
    [idleTimer invalidate];
    idleTimer = nil;
  }
}

@end
