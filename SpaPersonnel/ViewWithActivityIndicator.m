//
//  ViewWithActivityIndicator.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/30/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ViewWithActivityIndicator.h"
#import "ActivityView.h"
#import "UIView+SpaPersonnel.h"

@implementation ViewWithActivityIndicator {
  ActivityView *activityView;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    [self setupActivityView];
  }
  return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
  if (self = [super initWithFrame:frame]) {
    [self setupActivityView];
  }
  return self;
}

- (void)setupActivityView {
  ActivityView *aView = [ActivityView loadFromNibNamed:@"ActivityView"];
  aView.translatesAutoresizingMaskIntoConstraints = NO;
  aView.hidden = YES;
  [self addSubview:aView];
  
  [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[aView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(aView)]];
  [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[aView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(aView)]];
  
  [self layoutIfNeeded];
  
  activityView = aView;
}

- (void)lockInputAndShowActivityIndicator {
  activityView.hidden = NO;
}

- (void)unlockInputAndHideActivityIndicator {
  activityView.hidden = YES;
}

@end
