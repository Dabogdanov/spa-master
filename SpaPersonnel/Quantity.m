//
//  Quantity.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/25/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Quantity.h"

@implementation Quantity {
  NSDecimalNumber *value;
}

+ (instancetype)defaultQuantity {
  return [self quantityWithDecimalNumber:[self defaultValue]];
}

+ (instancetype)quantityWithDecimalNumber:(NSDecimalNumber *)decimalNumber {
  Quantity *newQuantity = nil;
  BOOL validationResult = [self validateDecimalNumber:decimalNumber];
  if (validationResult) {
    newQuantity = [[Quantity alloc] initWithDecimalNumber:decimalNumber];
  }
  return newQuantity;
}

+ (BOOL)validateDecimalNumber:(NSDecimalNumber *)decimalNumber {
  BOOL validationResult = NO;
  if (decimalNumber) {
    BOOL isNotLowerThanLowerLimit = ([decimalNumber compare:[self lowestPossibleValue]] != NSOrderedAscending);
    BOOL isNotHigherThanHigherLimit = ([decimalNumber compare:[self highestPossibleValue]] != NSOrderedDescending);
    
    NSUInteger digitsAfterDecimalPoint = [self numberOfDigitsAfterDecimalPoint];
    NSDecimalNumberHandler *behavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown
                                                                                              scale:(short)digitsAfterDecimalPoint
                                                                                   raiseOnExactness:NO
                                                                                    raiseOnOverflow:NO
                                                                                   raiseOnUnderflow:NO
                                                                                raiseOnDivideByZero:NO];
    NSDecimalNumber *floorInput = [decimalNumber decimalNumberByRoundingAccordingToBehavior:behavior];
    BOOL hasNoMoreDigitsAfterPointThanAllowed = [floorInput isEqualToNumber:decimalNumber];

    validationResult = isNotLowerThanLowerLimit && isNotHigherThanHigherLimit && hasNoMoreDigitsAfterPointThanAllowed;
  }
  return validationResult;
}

- (instancetype)initWithDecimalNumber:(NSDecimalNumber *)decimalNumber {
  if (self = [super init]) {
    value = decimalNumber;
  }
  return self;
}

- (BOOL)isEqual:(id)object {
  BOOL isEqual = NO;
  if ([object isKindOfClass:[self class]]) {
    Quantity *other = object;
    isEqual = [[other decimalValue] isEqualToNumber:[self decimalValue]];
  }
  return isEqual;
}

- (NSDecimalNumber *)decimalValue {
  return value;
}

- (NSString *)description {
  return [NSString stringWithFormat:@"%@", [self decimalValue]];
}

- (Quantity *)quantityByAddingOne {
  NSDecimalNumber *valuePlusOne = [[self decimalValue] decimalNumberByAdding:[NSDecimalNumber one]];
  NSDecimalNumberHandler *behavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown
                                                                                            scale:0
                                                                                 raiseOnExactness:NO
                                                                                  raiseOnOverflow:NO
                                                                                 raiseOnUnderflow:NO
                                                                              raiseOnDivideByZero:NO];
  NSDecimalNumber *nearestInteger = [valuePlusOne decimalNumberByRoundingAccordingToBehavior:behavior];
  
  return [Quantity quantityWithDecimalNumber:nearestInteger];
}

- (Quantity *)quantityBySubtractingOne {
  NSDecimalNumber *valueMinusOne = [[self decimalValue] decimalNumberBySubtracting:[NSDecimalNumber one]];
  NSDecimalNumberHandler *behavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundUp
                                                                                            scale:0
                                                                                 raiseOnExactness:NO
                                                                                  raiseOnOverflow:NO
                                                                                 raiseOnUnderflow:NO
                                                                              raiseOnDivideByZero:NO];
  NSDecimalNumber *nearestInteger = [valueMinusOne decimalNumberByRoundingAccordingToBehavior:behavior];
  
  return [Quantity quantityWithDecimalNumber:nearestInteger];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
  NSDecimalNumber *valueCopy = [[self decimalValue] copyWithZone:zone];
  id copy = [[self class] quantityWithDecimalNumber:valueCopy];
  return copy;
}

#pragma mark - Value Bounds

+ (NSDecimalNumber *)defaultValue {
  unsigned long long mantissa = 1ull;
  short exponent = (short)0;
  return [NSDecimalNumber decimalNumberWithMantissa:mantissa exponent:exponent isNegative:NO];
}

+ (NSDecimalNumber *)lowestPossibleValue {
  unsigned long long mantissa = 1ull;
  short exponent = -((short)[self numberOfDigitsAfterDecimalPoint]);
  return [NSDecimalNumber decimalNumberWithMantissa:mantissa exponent:exponent isNegative:NO];
}

+ (NSDecimalNumber *)highestPossibleValue {
  unsigned long long mantissa = 1ull;
  short exponent = (short)2;
  return [NSDecimalNumber decimalNumberWithMantissa:mantissa exponent:exponent isNegative:NO];
}

+ (NSUInteger)numberOfDigitsAfterDecimalPoint {
  return 3u;
}

@end
