//
//  ServicesFactory.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/11/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Servant.h"
#import "ScheduleProvider.h"
#import "OrderManager.h"
#import "Order.h"
#import "CatalogCacheManager.h"
#import "Customer.h"
#import "CustomerManager.h"

@interface ServicesFactory : NSObject

@property (readonly) Servant *servant;

- (void)authWithKey:(NSString *)key
         completion:(void (^)(NSError *error))completion;
- (void)logout;

- (ScheduleProvider *)newScheduleProvider;
- (OrderManager *)newOrderManagerForOrder:(Order *)order;
- (CatalogCacheManager *)newCatalogCacheManager;
- (CustomerManager *)newCustomerManagerForCustomer:(Customer *)customer;

@end
