//
//  NSString+SpaPersonnel.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/19/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Customer.h"

@interface NSString (SpaPersonnel)

+ (NSString *)sexAndAgeStringForCustomer:(Customer *)customer;

+ (NSString *)suffixForAge:(NSInteger)age;

+ (NSString *)clietFullNameStringForCustomer:(Customer *)customer;

@end
