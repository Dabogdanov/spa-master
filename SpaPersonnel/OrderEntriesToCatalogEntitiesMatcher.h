//
//  OrderEntriesToCatalogEntitiesMatcher.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/2/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "CatalogCacheManager.h"

@interface OrderEntriesToCatalogEntitiesMatcher : NSObject
- (instancetype)initWithCatalogCacheManager:(CatalogCacheManager *)catalogCacheManager;

- (void)matchConfirmedOrderEntries:(NSArray *)entries withCompletion:(void (^)(NSError *))completion;

@end
