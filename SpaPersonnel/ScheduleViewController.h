//
//  ScheduleViewController.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/26/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServicesFactory.h"
#import "DayPickerView.h"
#import "ScheduleItem.h"

@class ScheduleViewController;

@protocol ScheduleViewControllerDelegate <NSObject>
- (void)scheduleViewController:(ScheduleViewController *)scheduleViewController didSelectScheduleItem:(ScheduleItem *)item;
- (void)scheduleViewController:(ScheduleViewController *)scheduleViewController didSelectDate:(NSDate *)date;

@end

@interface ScheduleViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, DayPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet DayPickerView *dayPickerView;
@property (strong, nonatomic) ServicesFactory *services;
@property (weak, nonatomic) id <ScheduleViewControllerDelegate> delegate;
@property (readonly) NSDate *selectedDate;
@property (readonly) NSArray *entries;
@property (readonly) ScheduleItem *selectedItem;

- (void)updateWithScheduleItems:(NSArray *)scheduleItems selectedItem:(ScheduleItem *)itemToSelect shouldIndicateLoading:(BOOL)shouldIndicateLoading;

@end
