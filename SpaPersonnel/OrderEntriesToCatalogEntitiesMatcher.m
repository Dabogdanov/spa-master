//
//  OrderEntriesToCatalogEntitiesMatcher.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/2/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "OrderEntriesToCatalogEntitiesMatcher.h"
#import "ConfirmedOrderEntry.h"

@implementation OrderEntriesToCatalogEntitiesMatcher {
  CatalogCacheManager *cacheManager;
  NSInteger remainingEntries;
}

- (instancetype)initWithCatalogCacheManager:(CatalogCacheManager *)catalogCacheManager {
  if (self = [super init]) {
    cacheManager = catalogCacheManager;
  }
  return self;
}

- (void)matchConfirmedOrderEntries:(NSArray *)entries withCompletion:(void (^)(NSError *))completion {
  remainingEntries = entries.count;
  
  // if is required since completion block may change entries array passed here
  if (entries.count) {
    [cacheManager checkForUpdatesWithCompletion:^(BOOL success, NSError *error) {
      if (success) {
        for (ConfirmedOrderEntry *entry in entries) {
          [cacheManager fetchCachedCatalogEntityWithID:entry.itemID completion:^(CatalogEntity *entity, NSError *error) {
            entry.item = entity;
            remainingEntries--;
            if (!remainingEntries) {
              completion(error);
            }
          }];
        }
      }
    }];
  } else {
    completion(nil);
  }
}

@end
