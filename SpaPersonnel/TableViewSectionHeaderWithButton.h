//
//  TableViewSectionHeaderWithButton.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/4/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface TableViewSectionHeaderWithButton : UIView
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@end
