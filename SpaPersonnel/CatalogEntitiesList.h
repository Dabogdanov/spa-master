//
//  CatalogEntitiesList.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//


@interface CatalogEntitiesList : NSObject
@property (copy, nonatomic) NSArray *catalogEntities;
@property (copy, nonatomic) NSString *catalogVersion;

@end
