//
//  SpaButton.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/2/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "SpaButton.h"
#import "UIColor+SpaPersonnel.h"

@implementation SpaButton

- (CGFloat)cornerRadius {
  return 5.0f;
}

- (instancetype)initWithFrame:(CGRect)frame {
  if (self = [super initWithFrame:frame]) {
    [self setupOnInit];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    [self setupOnInit];
  }
  return self;
}

- (void)setHighlighted:(BOOL)highlighted {
  [super setHighlighted:highlighted];
  [self updateWithState:self.state];
}

- (void)setEnabled:(BOOL)enabled {
  [super setEnabled:enabled];
  [self updateWithState:self.state];
}

- (void)layoutSubviews {
  [super layoutSubviews];
  [self updateShadowPath];
}

- (void)setupOnInit {
  // round corners
  self.layer.cornerRadius = [self cornerRadius];
  
  // drawing shadow
  self.layer.masksToBounds = NO;
  
  self.layer.shouldRasterize = YES;
  self.layer.rasterizationScale = [UIScreen mainScreen].scale;
  
  [self updateShadowPath];
  [self updateWithState:UIControlStateNormal];
}

- (void)updateShadowPath {
  UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRoundedRect:[self bounds]
                                                        cornerRadius:self.layer.cornerRadius];
  self.layer.shadowPath = shadowPath.CGPath;
}

- (void)updateWithState:(UIControlState)state {
  if (state == UIControlStateHighlighted) {
    self.backgroundColor = [UIColor colorWithHex:0xf1f1f1];
    
    self.layer.shadowColor = [UIColor colorWithHex:0x000000].CGColor;
    self.layer.shadowOpacity = 0.5f;
    self.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    self.layer.shadowRadius = 1.0f;
  } else {
    self.backgroundColor = [UIColor colorWithHex:0xffffff];
    
    self.layer.shadowColor = [UIColor colorWithHex:0x000000].CGColor;
    self.layer.shadowOpacity = 0.75f;
    self.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.layer.shadowRadius = 1.0f;
  }
}

@end
