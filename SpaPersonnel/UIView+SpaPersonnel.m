//
//  SpaView.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/3/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "UIView+SpaPersonnel.h"

@implementation UIView (SpaPersonnel)

- (void)makeCornersRound {
  self.layer.cornerRadius = 5.0f;
  self.layer.masksToBounds = YES;
}

+ (id)loadFromNibNamed:(NSString *)nibName {
  UIView *theView = nil;
  if (nibName) {
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    for (UIView *view in views) {
      if ([view isKindOfClass:[self class]]) {
        theView = view;
        break;
      }
    }
  }
  return theView;
}

@end