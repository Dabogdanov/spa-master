//
//  SpaModalPresentationController.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/19/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@protocol PresentedViewOffsetDataSource <NSObject>
@property (assign, nonatomic) CGPoint frameOffset;

@end


@interface SpaModalPresentationController : UIPresentationController
@property (weak, nonatomic) NSObject <PresentedViewOffsetDataSource> *offsetDataSource;

@end
