//
//  RecommendedOrderEntriesViewController.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/15/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "RecommendedOrderEntriesViewController.h"
#import "OrderManager.h"
#import "OrderTableModelElement.h"
#import "RecommendedOrderEntryTableViewCell.h"
#import "ConfirmedOrderEntry.h"
#import "AddNewOrderEntryViewController.h"
#import "Quantity.h"
#import "UIAlertView+SpaPersonnel.h"
#import "CommentaryViewController.h"

static NSString * const RecommendedOrderEntryCellID = @"recommendedOrderEntryCellId";
static NSString * const ModifyOrderEntryCellID = @"modifyOrderEntryCellId";
static NSString * const OrderEntryCommentControllerID = @"orderEntryCommentary";

static NSString * const EditOrderEntrySegueIdentifier = @"editOrderEntry";

typedef NS_ENUM(NSInteger, OrderTableModelStatus) {
  OrderTableModelStatusIsNotModifyingAnyElement = 0,
  OrderTableModelStatusIsModifyingElement,
  OrderTableModelStatusIsAddingElement
};

@interface RecommendedOrderEntriesViewController () <RecommendedOrderEntryTableViewCellDelegate, AddNewOrderEntryViewControllerDelegate, UIPopoverControllerDelegate>
@property (copy, nonatomic) void (^onAlertConfirm)();

@end

@implementation RecommendedOrderEntriesViewController {
  OrderManager *orderManager;
  Order *currentOrder;
  NSMutableArray *tableModel;
  NSNumberFormatter *quantityFormatter;
  UITextField *firstResponderTextField;
  UIAlertView *confirmStatusChangeAlertView;
  UIPopoverController *popover;
  
  OrderTableModelStatus modelStatus;
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
  [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidBeginEditingNotification object:nil];
  [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidEndEditingNotification object:nil];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveKeyboardDidShowNotification:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveKeyboardWillHideShowNotification:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveTextFieldTextDidBeginEditingNotification:) name:UITextFieldTextDidBeginEditingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveTextFieldTextDidEndEditingNotification:) name:UITextFieldTextDidEndEditingNotification object:nil];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  UINib *recommendedCellNib = [UINib nibWithNibName:@"RecommendedOrderEntryTableViewCell" bundle:nil];
  [self.tableView registerNib:recommendedCellNib forCellReuseIdentifier:RecommendedOrderEntryCellID];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

- (void)fillWithOrder:(Order *)order {
  [self abortEntryModification];
  currentOrder = order;
  
  orderManager = [self.services newOrderManagerForOrder:order];
  tableModel = [self tableModelWithOrder:order];
  headerView.hidden = !order;
  [self.tableView reloadData];
}

- (NSMutableArray *)tableModelWithOrder:(Order *)order {
  NSMutableArray *newRecommendedElements = [NSMutableArray new];
  for (ConfirmedOrderEntry *entry in order.orderEntries) {
    OrderTableModelElement *element = [[OrderTableModelElement alloc] initWithConfirmedOrderEntry:entry];
    if (!element.entry.isCurrent && (element.entry.status != Cancelled)) {
      [newRecommendedElements addObject:element];
    }
  }
  return newRecommendedElements;
}

#pragma mark - Handling Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:EditOrderEntrySegueIdentifier]) {
    NSDictionary *params = (NSDictionary *)sender;
    ConfirmedOrderEntry *entry = (ConfirmedOrderEntry *)params[@"entry"];
    NSString *title = (NSString *)params[@"title"];
    [self setupAddNewOrderViewControllerWithHierarchyRootController:segue.destinationViewController orderEntry:entry title:title];
  }
}

- (void)setupAddNewOrderViewControllerWithHierarchyRootController:(UIViewController *)rootController orderEntry:(ConfirmedOrderEntry *)entry title:(NSString *) title {
  AddNewOrderEntryViewController *editOrderEntryController;
  if ([rootController isKindOfClass:[UINavigationController class]]) {
    UINavigationController *navController = (UINavigationController *)rootController;
    editOrderEntryController = (AddNewOrderEntryViewController *)navController.viewControllers[0];
  } else {
    editOrderEntryController = (AddNewOrderEntryViewController *)rootController;
  }
  editOrderEntryController.title = title;
  editOrderEntryController.services = self.services;
  [editOrderEntryController fillWithOrderEntry:entry];
  editOrderEntryController.delegate = self;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return tableModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell;
  OrderTableModelElement *element = tableModel[indexPath.row];
  ConfirmedOrderEntry *entry = element.entry;
  cell = [self cellForRecommendedOrderEntry:entry inTableView:tableView];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}

- (RecommendedOrderEntryTableViewCell *)cellForRecommendedOrderEntry:(ConfirmedOrderEntry *)entry inTableView:(UITableView *)aTableView {
  RecommendedOrderEntryTableViewCell *recommendedEntryCell = [aTableView dequeueReusableCellWithIdentifier:RecommendedOrderEntryCellID];
  recommendedEntryCell.delegate = self;
  [recommendedEntryCell setCancelOn];
  [recommendedEntryCell setEditOn];
  [recommendedEntryCell setQuantity:entry.quantity];

  BOOL commentIsAvailable = (entry.comment.length > 0);
  if (commentIsAvailable) {
    [recommendedEntryCell setInfoOn];
  } else {
    [recommendedEntryCell setInfoOff];
  }

  recommendedEntryCell.topLabel.text = entry.item ? entry.item.name : [NSString stringWithFormat:@"itemID: %lld", entry.itemID];
  [recommendedEntryCell setButtonsEnabled:YES];
  return recommendedEntryCell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 75.0f;
}

#pragma mark - Notifications

- (void)didReceiveKeyboardDidShowNotification:(NSNotification *)notification {
  if (firstResponderTextField) {
    NSDictionary *info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;

    // If active text field is hidden by keyboard, scroll it so it's visible
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
    CGRect firstResponderRect = [self.tableView convertRect:firstResponderTextField.frame fromView:firstResponderTextField.superview];
    CGPoint origin = firstResponderRect.origin;
    origin.y -= self.tableView.contentOffset.y;
    if (!CGRectContainsPoint(aRect, origin) ) {
      CGPoint scrollPoint = CGPointMake(0.0, firstResponderRect.origin.y-(aRect.size.height));
      [self.tableView setContentOffset:scrollPoint animated:YES];
    }
  }
}

- (void)didReceiveKeyboardWillHideShowNotification:(NSNotification *)notification {
  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
  self.tableView.contentInset = contentInsets;
  self.tableView.scrollIndicatorInsets = contentInsets;
}

- (void)didReceiveTextFieldTextDidBeginEditingNotification:(NSNotification *)notification {
  UITextField *textField = notification.object;
  if ([textField isDescendantOfView:self.view]) {
    firstResponderTextField = notification.object;
  }
}

- (void)didReceiveTextFieldTextDidEndEditingNotification:(NSNotification *)notification {
  firstResponderTextField = nil;
}

#pragma mark - Actions

- (IBAction)addOrderEntry:(id)sender {
  [self initEntryCreation];
  [self.tableView reloadData];
  [self performSelector:@selector(scrollRecommendedTableToTopRow) withObject:nil afterDelay:0.2];
}

#pragma mark - RecommendedOrderEntryTableViewCellDelegate

- (void)recommendedOrderEntryTableViewCell:(RecommendedOrderEntryTableViewCell *)cell userDidPressInfoButton:(UIButton *)button {
  NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
  OrderTableModelElement *element = tableModel[indexPath.row];
  ConfirmedOrderEntry *entry = element.entry;
  
  UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:OrderEntryCommentControllerID];
  CommentaryViewController *commentaryController = (CommentaryViewController *)navigationController.viewControllers.firstObject;
  [commentaryController fillWithCommentary:entry.comment];
  
  popover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
  [popover presentPopoverFromRect:button.frame inView:button.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
  popover.delegate = self;
}

- (void)recommendedOrderEntryTableViewCellUserDidPressCancelButton:(RecommendedOrderEntryTableViewCell *)cell {
  [self displayConfirmationAlertWithConfirmationHandler:^{
    [cell setButtonsEnabled:NO];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    OrderTableModelElement *element = tableModel[indexPath.row];
    ConfirmedOrderEntry *entry = element.entry;
    
    [orderManager asyncDeleteEntryWithID:entry.ID completion:^(NSError *error) {
      dispatch_async(dispatch_get_main_queue(), ^{
        if (error) {
          [cell setButtonsEnabled:YES];
          [UIAlertView showAlertWithTitle:@"Ошибка при попытка изменения статуса" message:error.localizedDescription];
        } else {
          
          [self fillWithOrder:currentOrder];
          
          [self.delegate orderViewControllerDidChangeOrder:self];
        }
      });
    }];
    
  }];
}

- (void)recommendedOrderEntryTableViewCellUserDidPressEditButton:(RecommendedOrderEntryTableViewCell *)cell {
  NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
  NSIndexPath *ip = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
  [self startEditEntryAtIndexPath:ip];
  [self.tableView reloadData];
  
  [self performSelector:@selector(scrollrecommendedTableToRowAtIndexPath:) withObject:indexPath afterDelay:0.2];
}


- (void)makeRequestToAddOrderEntryWithEntryModel:(ConfirmedOrderEntry *)entry completion:(void (^)())completion {
  [orderManager asyncAddOrderEntryWithItemID:entry.itemID commentary:entry.comment quantity:entry.quantity completion:^(NSError *error) {
    dispatch_async(dispatch_get_main_queue(), ^{
      completion();
      if (error) {
        [UIAlertView showAlertWithTitle:@"Ошибка при отправке рекомендации" message:error.localizedDescription];
      } else {
        
        [self fillWithOrder:currentOrder];
        
        [self.delegate orderViewControllerDidMadeOrderChangeRequiringFullUpdate:self];
      }
    });
  }];
}

- (void)makeRequestToModifyOrderEntryWithEntryModel:(ConfirmedOrderEntry *)entry completion:(void (^)())completion {
  [orderManager asyncEditEntryWithID:entry.ID
                           newItemID:entry.itemID
                       newCommentary:entry.comment
                         newQuantity:entry.quantity
                           newStatus:entry.status
                          completion:^(NSError *error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                              completion();
                              if (error) {
                                [UIAlertView showAlertWithTitle:@"Ошибка при изменении рекомендации" message:error.localizedDescription];
                              } else {
                                
                                [self fillWithOrder:currentOrder];
                                
                                [self.delegate orderViewControllerDidChangeOrder:self];
                              }
                            });
                          }];
}

#pragma mark - AddNewOrderEntryViewControllerDelegate

- (void)addNewOrderEntryViewControllerUserWantsToDissmiss:(AddNewOrderEntryViewController *)controller {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)addNewOrderEntryViewController:(AddNewOrderEntryViewController *)controller userDidModifyOrderEntry:(ConfirmedOrderEntry *)orderEntry {
  if ([self modelModifyingStatus] == OrderTableModelStatusIsModifyingElement) {
    [self makeRequestToModifyOrderEntryWithEntryModel:orderEntry completion:^{
      [self dismissViewControllerAnimated:YES completion:nil];
    }];
  } else if ([self modelModifyingStatus] == OrderTableModelStatusIsAddingElement) {
    [self makeRequestToAddOrderEntryWithEntryModel:orderEntry completion:^{
      [self dismissViewControllerAnimated:YES completion:nil];
    }];
  }
}

#pragma mark - UIAlertViewDelegate

- (void)displayConfirmationAlertWithConfirmationHandler:(void (^)())confirmHandler {
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Вы уверены?" message:nil delegate:self cancelButtonTitle:@"Нет" otherButtonTitles:@"Да", nil];
  confirmStatusChangeAlertView = alertView;
  
  RecommendedOrderEntriesViewController * __weak safeSelfReference = self;
  self.onAlertConfirm = ^{
    confirmHandler();
    safeSelfReference.onAlertConfirm = nil;
  };
  [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if ([confirmStatusChangeAlertView isEqual:alertView]) {
    if (buttonIndex != [alertView cancelButtonIndex]) {
      self.onAlertConfirm();
      confirmStatusChangeAlertView = nil;
    }
  }
}

#pragma mark - UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
  popover = nil;
}

#pragma mark - Formatters

- (NSNumberFormatter *)quantityFormatter { // TODO: exactly the same used in controller, refactor
  if (!quantityFormatter) {
    quantityFormatter = [[NSNumberFormatter alloc] init];
    quantityFormatter.locale = [NSLocale currentLocale];
  }
  return quantityFormatter;
}

#pragma mark - Scrolling Table

- (void)scrollRecommendedTableToTopRow {
  if (tableModel.count) {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
  }
}

- (void)scrollrecommendedTableToRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.row < tableModel.count) {
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
  }
}

#pragma mark - Working With Table Model

- (void)startEditEntryAtIndexPath:(NSIndexPath *)indexPath {
  [self abortEntryModification];
  
  modelStatus = OrderTableModelStatusIsModifyingElement;
  
  OrderTableModelElement *element = tableModel[indexPath.row];
  ConfirmedOrderEntry *entry = [self copyOfExistingOrderEntry:element.entry];
  
  [self performSegueWithIdentifier:EditOrderEntrySegueIdentifier sender:@{@"entry" : entry, @"title" : @"Изменить"}];
}

- (void)initEntryCreation {
  [self abortEntryModification];
  
  ConfirmedOrderEntry *entry = [self copyOfExistingOrderEntry:nil];
  entry.orderID = currentOrder.ID;
  
  modelStatus = OrderTableModelStatusIsAddingElement;
  
  [self performSegueWithIdentifier:EditOrderEntrySegueIdentifier sender:@{@"entry" : entry, @"title" : @"Добавить"}];
}

- (void)abortEntryModification {
  modelStatus = OrderTableModelStatusIsNotModifyingAnyElement;
}

- (OrderTableModelStatus)modelModifyingStatus {
  return modelStatus;
}

- (ConfirmedOrderEntry *)copyOfExistingOrderEntry:(ConfirmedOrderEntry *)entry {
  ConfirmedOrderEntry *entryCopy = [[ConfirmedOrderEntry alloc] init];
  entryCopy.item = entry.item;
  entryCopy.customer = entry.customer;
  entryCopy.servant = entry.servant;
  entryCopy.orderID = entry.orderID;
  entryCopy.itemID = entry.itemID;
  entryCopy.comment = entry.comment;
  
  Quantity *quantity = [Quantity quantityWithDecimalNumber:entry.quantity];
  if (!quantity) {
    entryCopy.quantity = [[Quantity defaultQuantity] decimalValue];
  } else {
    entryCopy.quantity = entry.quantity;
  }
  entryCopy.status = entry.status;
  entryCopy.ID = entry.ID;
  return entryCopy;
}

@end
