//
//  AuthorizationManager.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServerManager.h"
#import "Session.h"

@interface AuthorizationManager : NSObject
- (instancetype)initWithServerManager:(ServerManager *)serverManager;
- (void)authWithKey:(NSString *)key
         completion:(void (^)(Session *session, NSError *error))completion;

@end
