//
//  OrderEntriesArrayBuilder.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/1/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "OrderEntriesArrayBuilder.h"
#import "ConfirmedOrderEntry.h"
#import "CatalogCacheManager.h"
#import "OrderEntriesToCatalogEntitiesMatcher.h"

@implementation OrderEntriesArrayBuilder {
  NSDate *startDate;
  NSDate *endDate;
  NSCalendar *_UTCCalendar;
  
  NSInteger responsesLeft;
  
  NSMutableArray *accumulatedEntries;

  OrderEntriesToCatalogEntitiesMatcher *matcher;
}

- (instancetype)initWithStartDate:(NSDate *)aStartDate endDate:(NSDate *)anEndDate {
  if (self = [super init]) {
    startDate = aStartDate;
    endDate = anEndDate;
    accumulatedEntries = [NSMutableArray new];
  }
  return self;
}

- (void)build {
  NSArray *requestDates = [self requestDatesToCoverGivenDateInterval];
  responsesLeft = requestDates.count;
  for (NSDate *date in requestDates) {
    [self.serverManager fetchOrderEntriesForDate:date
                                         success:^(NSArray *orderEntries) {
                                           [self handleEntriesResponse:orderEntries];
                                         }
                                         failure:^(NSError *error) {
                                           [self handleFailureWithError:error];
                                         }];
  }
}

#pragma mark Calculating Requests Parameters

// refactor (separate time service class)
- (NSArray *)requestDatesToCoverGivenDateInterval {
  NSDate *startDateDayStartUTC = [self dayStartUTCFromDate:startDate];
  NSDate *endDateDayStartUTC = [self dayStartUTCFromDate:endDate];
  NSDateComponents *dayCountComps = [[self UTCCalendar] components:NSCalendarUnitDay fromDate:startDateDayStartUTC toDate:endDateDayStartUTC options:0];
  NSInteger additionalDaysCount = dayCountComps.day;
  NSMutableArray *requestDates = [NSMutableArray arrayWithObject:[self dayStartUTCFromDate:startDate]];
  for (NSInteger i = 0; i < additionalDaysCount; i++) {
    NSDate *lastDate = (NSDate *)requestDates.lastObject;
    NSDate *nextDay = [self nextDayUTCForDate:lastDate];
    [requestDates addObject:nextDay];
  }
  return [requestDates copy];
}

- (NSCalendar *)UTCCalendar {
  if (!_UTCCalendar) {
    _UTCCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    _UTCCalendar.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
  }
  return _UTCCalendar;
}

- (NSDateComponents *)dayComponentsFromDate:(NSDate *)date {
  return [[self UTCCalendar] components:NSCalendarUnitDay | NSCalendarUnitYear | NSCalendarUnitMonth fromDate:date];
}

- (NSDate *)dayStartUTCFromDate:(NSDate *)date {
  NSDateComponents *comps = [[self UTCCalendar] components:NSCalendarUnitDay | NSCalendarUnitYear | NSCalendarUnitMonth fromDate:date];
  NSDate *dayStart = [[self UTCCalendar] dateFromComponents:comps];
  return dayStart;
}

- (NSDate *)nextDayUTCForDate:(NSDate *)date {
  NSDateComponents *comps = [[self UTCCalendar] components:NSCalendarUnitDay | NSCalendarUnitYear | NSCalendarUnitMonth fromDate:date];
  comps.day += 1;
  return [[self UTCCalendar] dateFromComponents:comps];
}

#pragma mark Handling Reponse

- (void)handleEntriesResponse:(NSArray *)entries {
  if (responsesLeft > 0) {
    [accumulatedEntries addObjectsFromArray:entries];
    
    responsesLeft--;
    if (responsesLeft == 0) {
      
      CatalogCacheManager *cacheManager = [[CatalogCacheManager alloc] initWithServerManager:self.serverManager];
      matcher = [[OrderEntriesToCatalogEntitiesMatcher alloc] initWithCatalogCacheManager:cacheManager];
      [matcher matchConfirmedOrderEntries:accumulatedEntries withCompletion:^(NSError *error) {
        matcher = nil;
        NSArray *filteredArray = [self arrayByRemovingExtraEntriesFromArray:accumulatedEntries];
        [self.delegate orderEntriesArrayBuilder:self didBuildNewEntriesArray:filteredArray];
        [accumulatedEntries removeAllObjects];
      }];
    }
  }
}

- (void)handleFailureWithError:(NSError *)error {
  [self.delegate orderEntriesArrayBuilder:self didFailToBuildEntriesArray:error];
}

#pragma mark Removing Extra Entries

- (NSArray *)arrayByRemovingExtraEntriesFromArray:(NSArray *)entries {
  NSMutableArray *remainingEntries = [NSMutableArray new];
  for (ConfirmedOrderEntry *entry in [entries copy]) {
    if (((!startDate) ||
        ([startDate compare:entry.startDate] != NSOrderedDescending &&
         [endDate compare:entry.startDate] != NSOrderedAscending)) &&
        (entry.status >= 0) /*&&
        (entry.status < OrderEntryStatusCount)*/) {
      [remainingEntries addObject:entry];
    }
  }
  return [remainingEntries copy];
}

@end
