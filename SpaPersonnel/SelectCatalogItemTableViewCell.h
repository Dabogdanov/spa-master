//
//  SelectCatalogItemTableViewCell.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "SelectCatalogItemView.h"

@interface SelectCatalogItemTableViewCell : UITableViewCell {
  IBOutlet UILabel * __weak itemNameLabel;
}
@property (weak, nonatomic) SelectCatalogItemView *theContentView;

- (void)setItemName:(NSString *)name;

@end
