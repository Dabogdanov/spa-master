//
//  UIColor+SpaPersonnel.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/28/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "UIColor+SpaPersonnel.h"

@implementation UIColor (SpaPersonnel)

#pragma mark - Utility

+ (UIColor *)colorWithRed255:(CGFloat)red green255:(CGFloat)green blue255:(CGFloat)blue alpha:(CGFloat)alpha {
  return [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:alpha];
}

+ (UIColor *)colorWithHex:(int)hex {
  return [self colorWithHex:hex alpha:1.0f];
}

+ (UIColor *)colorWithHex:(int)hex alpha:(CGFloat)alpha {
  return [UIColor colorWithRed255:((0xFF0000 & hex) >> 16) green255:((0x00FF00 & hex) >> 8) blue255:(0x0000FF & hex) alpha:alpha];
}

+ (UIColor *) appMainColor{
    return [UIColor colorWithHex: 0xd29847];
}

#pragma mark - Schedule

+ (UIColor *)activeScheduleItemColor {
  return [UIColor colorWithHex:0xbaddbb];
}

+ (UIColor *)inactiveScheduleItemColor {
  return [UIColor colorWithHex:0xeeeeee];
}

+ (UIColor *)awaitingForConfirmationScheduleItemColor {
  return [UIColor colorWithHex:0xffd635];
}

+ (UIColor *)selectedScheduleItemColor {
  return [UIColor colorWithHex:0xffd635];
}

+ (UIColor *)selectedScheduleItemBorderColor {
  return [UIColor colorWithHex:0x26a8ff];
}

#pragma mark - Buttons Color

+ (UIColor *)defaultButtonTextColor {
  return [UIColor colorWithHex:0x26a8ff];
}

+ (UIColor *)highlightedButtonTextColor {
  return [UIColor colorWithHex:0x6043bb alpha:0.5f];
}

#pragma mark - Value Label Color

+ (UIColor *)valueLabelBackgroundColor {
  return nil;
}

+ (UIColor *)valueLabelTextColor {
  return nil;
}

+ (UIColor *)buttonDefaultRedColor {
  return [UIColor colorWithHex:0xff3a2f];
}

+ (UIColor *)buttonHighlightedRedColor {
  return [UIColor colorWithHex:0xff3a2f alpha:0.5];
}

#pragma mark - Views Background

+ (UIColor *)mainScreenBackgroundColor {
  return [UIColor colorWithHex:0xfffbf1];
}

+ (UIColor *)navigationItemBackgroundColor {
    return [UIColor blackColor]; //[UIColor colorWithHex:0x585c59];
}

+ (UIColor *)navigationItemTintColor {
  return [UIColor colorWithHex:0x26a8ff];
}

#pragma mark - Order Screen

+ (UIColor *)orderScreenHeaderColor {
  return [UIColor colorWithHex:0xb9c1bb];
}

+ (UIColor *)orderScreenSectionHeaderColor {
  return [UIColor mainScreenBackgroundColor];
}

+ (UIColor *)orderScreenItemBackgroudColor {
  return [UIColor colorWithHex:0xf6f6f6];
}

+ (UIColor *)orderScreenItemIsBeginProcessedBackgroudColor {
  return [UIColor colorWithHex:0xfcfade];
}

+ (UIColor *)dividerLineColor {
  return [UIColor colorWithHex:0x575b58];
}

@end
