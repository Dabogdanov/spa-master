//
//  Digit.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/18/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "DecimalDigit.h"

@interface DecimalDigit ()
@property (assign, nonatomic) NSUInteger digit;

@end

@implementation DecimalDigit

+ (instancetype)decimalDigitWithUnsignedInteger:(NSUInteger)aUInt {
  DecimalDigit *newDecimalDigit = nil;
  if (aUInt <= 9) {
    newDecimalDigit = [[DecimalDigit alloc] init];
    newDecimalDigit.digit = aUInt;
  }
  return newDecimalDigit;
}

- (NSUInteger)value {
  return self.digit;
}

- (NSString *)description {
  return [NSString stringWithFormat:@"%ld", (long)self.digit];
}

@end
