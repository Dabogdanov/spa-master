//
//  EditOrderEntryCommentView.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/23/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "EditOrderEntryCommentView.h"

@implementation EditOrderEntryCommentView

- (NSString *)comment {
  return textView.text;
}

- (void)setComment:(NSString *)comment {
  textView.text = comment;
}

- (void)setTextViewDelegate:(id<UITextViewDelegate>)delegate {
  textView.delegate = delegate;
}

- (CGSize)sizeThatFits:(CGSize)size {
  CGSize currentSize = self.frame.size;
  CGSize currentTextViewSize = textView.frame.size;
  CGSize optimumTextViewSize = [textView sizeThatFits:CGSizeMake(currentTextViewSize.width, CGFLOAT_MAX)];
  CGFloat optimumHeightDiff = currentTextViewSize.height - optimumTextViewSize.height;
  CGSize optimumSize = CGSizeMake(currentSize.width, currentSize.height - optimumHeightDiff);
  return optimumSize;
}

@end
