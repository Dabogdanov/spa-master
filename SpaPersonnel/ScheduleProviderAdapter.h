//
//  ScheduleProviderAdapter.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServicesFactory.h"

typedef void (^ScheduleProviderCompletion)(NSArray *schedule, NSDate *date, NSError *error);

@interface ScheduleProviderAdapter : NSObject
+ (ScheduleProviderAdapter *)asyncGetScheduleWithDate:(NSDate *)date completionBlock:(ScheduleProviderCompletion)completion servicesFactory:(ServicesFactory *)factory;

@end
