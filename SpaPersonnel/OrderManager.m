//
//  OrderManager.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/10/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "OrderManager.h"
#import "OrderEntriesToCatalogEntitiesMatcher.h"

@implementation OrderManager {
  ServerManager *serverManager;
  Order *_managedOrder;
  OrderEntriesToCatalogEntitiesMatcher *matcher;
}

- (instancetype)initWithOrder:(Order *)anOrder serverManager:(ServerManager *)aServerManager {
  if (self = [super init]) {
    _managedOrder = anOrder;
    serverManager = aServerManager;
  }
  return self;
}

- (void)asyncAddOrderEntryWithItemID:(int64_t)itemID
                          commentary:(NSString *)commentary
                            quantity:(NSDecimalNumber *)quantity
                          completion:(void (^)(NSError *))completion {
  OrderManager * __weak weakSelf =  self;
  [serverManager addOrderEntryToOrderWithID:_managedOrder.ID
                                 withItemID:itemID
                                    comment:commentary
                                   quantity:quantity
                                    success:^(ConfirmedOrderEntry *anEntry) {
                                  [weakSelf handleAddEntryResponse:anEntry withCompletion:completion];
                                } failure:^(NSError *error) {
                                  completion(error);
                                }];
}

- (void)handleAddEntryResponse:(ConfirmedOrderEntry *)anEntry withCompletion:(void (^)(NSError *))completion {
  [self matchEntry:anEntry toCatalogEntityWithCompletion:^{
    [_managedOrder addEntry:anEntry];
    completion(nil);
  }];
}

- (void)asyncDeleteEntryWithID:(int64_t)ID completion:(void (^)(NSError *))completion {
  [serverManager deleteOrderEntryWithID:ID success:^(ConfirmedOrderEntry *anEntry) {
    [self handleDeleteEntryResponse:anEntry withCompletion:completion];
  } failure:^(NSError *error) {
    completion(error);
  }];
}

- (void)handleDeleteEntryResponse:(ConfirmedOrderEntry *)anEntry withCompletion:(void (^)(NSError *))completion {
  [_managedOrder removeEntryWithID:anEntry.ID];
  completion(nil);
}

- (void)asyncEditEntryWithID:(int64_t)entryID
                   newItemID:(int64_t)itemID
               newCommentary:(NSString *)commentary
                 newQuantity:(NSDecimalNumber *)quantity
                   newStatus:(OrderEntryStatus)status
                  completion:(void (^)(NSError *))completion {
    [serverManager editOrderEntryWithID:entryID
                              newStatus:status
                              newItemID:itemID
                             newComment:commentary
                            newQuantity:quantity
                                success:^(ConfirmedOrderEntry *anEntry)
  {
    [self handleEditEntryResponse:anEntry completion:completion];
  } failure:^(NSError *error) {
    completion(error);
  }];
}

- (void)handleEditEntryResponse:(ConfirmedOrderEntry *)anEntry completion:(void (^)(NSError *error))completion {
  [self matchEntry:anEntry toCatalogEntityWithCompletion:^{
    [_managedOrder updateOldEntryWithNew:anEntry];
    completion(nil);
  }];
}

- (void)matchEntry:(ConfirmedOrderEntry *)entry toCatalogEntityWithCompletion:(void (^)())completion {
  CatalogCacheManager *cacheManager = [[CatalogCacheManager alloc] initWithServerManager:serverManager];
  matcher = [[OrderEntriesToCatalogEntitiesMatcher alloc] initWithCatalogCacheManager:cacheManager];
  [matcher matchConfirmedOrderEntries:@[entry] withCompletion:^(NSError *error) {
    matcher = nil;
    completion();
  }];
}

@end
