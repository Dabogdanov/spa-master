//
//  CommentaryViewController.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/26/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "CommentaryViewController.h"

@interface CommentaryViewController ()

@end

@implementation CommentaryViewController {
  NSString *commentary;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self setCommentaryText];
//  self.preferredContentSize = CGSizeMake(MAX(300.0f, preferredSize.width), MAX(200.0f, preferredSize.height));
  // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)fillWithCommentary:(NSString *)aCommentary {
  commentary = aCommentary;
  if (commentaryLabel) {
    [self setCommentaryText];
  }
}

- (void)setCommentaryText {
  commentaryLabel.text = commentary;
  CGSize preferredSize = [commentaryLabel sizeThatFits:CGSizeMake(300.0f, CGFLOAT_MAX)];
  self.preferredContentSize = CGSizeMake(300.0f, MIN(200.0f, preferredSize.height + 10.0f));
  [self.view layoutIfNeeded];
}

@end
