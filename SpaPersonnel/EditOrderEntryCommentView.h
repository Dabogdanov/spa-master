//
//  EditOrderEntryCommentView.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/23/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

@interface EditOrderEntryCommentView : UIView {
  IBOutlet UITextView * __weak textView;
}

- (NSString *)comment;
- (void)setComment:(NSString *)comment;

- (void)setTextViewDelegate:(id <UITextViewDelegate>)delegate;

@end
