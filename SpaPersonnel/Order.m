//
//  Order.m
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 11/24/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "Order.h"

@implementation Order {
  NSMutableArray *_orderEntries;
}

- (NSArray *)orderEntries {
  return _orderEntries;
}

- (void)addEntry:(ConfirmedOrderEntry *)entry {
  // TODO: same status validation that was implemented in OrderEntriesArrayBuilder, refactor
  if (YES/*(entry.status >= 0) &&
      (entry.status < OrderEntryStatusCount)*/) {
    if (!self.orderEntries) {
      _orderEntries = [NSMutableArray new];
    }
    [_orderEntries addObject:entry];
  }
}

- (void)updateOldEntryWithNew:(ConfirmedOrderEntry *)newEntry {
  NSUInteger anIndex = [_orderEntries indexOfObjectPassingTest:^BOOL(ConfirmedOrderEntry *oldEntry, NSUInteger idx, BOOL *stop) {
    return (oldEntry.ID == newEntry.ID);
  }];
  if (anIndex != NSNotFound) {
    [_orderEntries replaceObjectAtIndex:anIndex withObject:newEntry];
  } else {
    [self addEntry:newEntry];
  }
}

- (void)removeEntryWithID:(int64_t)entryID {
  NSUInteger anIndex = [_orderEntries indexOfObjectPassingTest:^BOOL(ConfirmedOrderEntry *oldEntry, NSUInteger idx, BOOL *stop) {
    return (oldEntry.ID == entryID);
  }];
  if (anIndex != NSNotFound) {
    [_orderEntries removeObjectAtIndex:anIndex];
  }
}

@end
