//
//  CatalogManager.h
//  SpaPersonnel
//
//  Created by Ilya Chistyakov on 12/2/14.
//  Copyright (c) 2014 Ilya Chistyakov. All rights reserved.
//

#import "ServerManager.h"
#import "CatalogEntity.h"

@interface CatalogCacheManager : NSObject
- (instancetype)initWithServerManager:(ServerManager *)aServerManager;

- (void)checkForUpdatesWithCompletion:(void (^)(BOOL success, NSError *error))completion;

- (void)fetchCachedCatalogEntityWithID:(int64_t)itemID completion:(void (^)(CatalogEntity *, NSError *))completion;

- (void)fetchCachedCatalogEntitiesWithCompletion:(void (^)(NSArray *entities, NSError *error))completion;

@end
